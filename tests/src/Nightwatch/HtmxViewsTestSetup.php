<?php

declare(strict_types=1);

namespace Drupal\Tests\htmx;

use Drupal\Core\Config\FileStorage;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Extension\ThemeInstallerInterface;
use Drupal\TestSite\TestSetupInterface;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Prep the site for a views test.
 */
class HtmxViewsTestSetup implements TestSetupInterface {

  use UserCreationTrait;

  /**
   * Views used by this test.
   *
   * @var string[]
   */
  protected array $testViews = ['test_htmx_content'];

  /**
   * {@inheritdoc}
   */
  public function setup() {
    // Install Olivero and set it as the default theme.
    $theme_installer = \Drupal::service('theme_installer');
    assert($theme_installer instanceof ThemeInstallerInterface);
    $theme_installer->install(['olivero'], TRUE);
    $system_theme_config = \Drupal::configFactory()
      ->getEditable('system.theme');
    $system_theme_config->set('default', 'olivero')->save();

    // Install required modules.
    $module_installer = \Drupal::service('module_installer');
    assert($module_installer instanceof ModuleInstallerInterface);
    $module_installer->install(['node', 'views', 'htmx', 'htmx_test_views']);

    // Insure correct permissions.
    $this->grantPermissions(Role::load(RoleInterface::ANONYMOUS_ID), ['access content']);

    // Create content for the view.
    // Create a Content type and eleven test nodes.
    $type = NodeType::create([
      'type' => 'page',
      'name' => 'page',
    ]);
    $type->save();
    node_add_body_field($type);

    for ($i = 1; $i <= 11; $i++) {
      $fields = [
        'type' => 'page',
        'uid' => 1,
        'title' => $i > 6 ? 'Node ' . $i . ' content' : 'Node ' . $i . ' content default_value',
        'changed' => $i * 1000,
      ];
      // Create node object.
      $node = Node::create($fields);
      $node->save();
    }
    // Now load views.
    if (!empty($this->testViews)) {
      $config_dir = \Drupal::service('extension.list.module')->getPath('htmx_test_views') . '/test_views';
      $file_storage = new FileStorage($config_dir);

      $available_views = $file_storage->listAll('views.view.');
      $storage = \Drupal::entityTypeManager()->getStorage('view');

      foreach ($this->testViews as $id) {
        $config_name = 'views.view.' . $id;
        if (in_array($config_name, $available_views)) {
          $storage
            ->create($file_storage->read($config_name))
            ->save();
        }
      }
    }

    // Rebuild the router once.
    \Drupal::service('router.builder')->rebuild();
  }

}
