// The javascript that creates dropbuttons is not present on the /page at
// initial load.  If the once data property is added then the JS was loaded
// and triggered on the inserted content.
module.exports = {
  '@tags': ['htmx'],
  before(browser) {
    browser
      .drupalInstall({
        setupFile: `${__dirname}/../HtmxViewsTestSetup.php`
      })
  },
  afterEach(browser) {
    browser.drupalLogAndEnd({ onlyOnError: true });
  },
  after(browser) {
    browser.drupalUninstall();
  },
  'HTMX Pager': browser => {
    browser
      .drupalRelativeURL('/test_htmx_content')
      .waitForElementVisible('div.view-id-test_htmx_content', 60000)
      .assert.textContains('div.view-id-test_htmx_content', 'Node 11 content')
      .waitForElementVisible('.pager__item--next button.htmx__pager--action-link', 60000)
      .click('.pager__item--next button.htmx__pager--action-link')
      .waitForElementVisible('.pager__item--previous button.htmx__pager--action-link', 60000)
      .assert.textContains('div.view-id-test_htmx_content', 'Node 1 content')
  },
};
