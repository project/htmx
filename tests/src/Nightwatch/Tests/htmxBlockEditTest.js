module.exports = {
  '@tags': ['htmx'],
  before(browser) {
    browser
      .drupalInstall({
        setupFile: `${__dirname}/../HtmxBlockPlacementTestSetup.php`
      })
  },
  afterEach(browser) {
    browser.drupalLogAndEnd({ onlyOnError: false });
  },
  after(browser) {
    browser.drupalUninstall();
  },
  'Dynamic Block': browser => {
    browser.drupalLoginAsAdmin(() => {
      browser
        .drupalRelativeURL('/admin/structure/htmx-block')
        .waitForElementVisible('body', 1000)
        .waitForElementVisible('a[href$="edit/htmx_test_block"]', 1000)
        .click('a[href$="edit/htmx_test_block"]')
        .getText({
          selector: 'span.ui-dialog-title',
          timeout: 2000 // overwrite the default timeout (in ms) to check if the element is present
        }, function (headerTitle) {
          browser.assert.equal(headerTitle.value, 'Configure an HTMX block');
        })
        .setValue('input[data-drupal-selector="edit-settings-label"]', 'Title is changed')
        .click('input[data-drupal-selector="edit-actions-submit"]')
        .waitForElementNotPresent('input[data-drupal-selector="edit-actions-submit"]', 1000);
      browser.assert.textEquals('#htmx-block-list td', 'Title is changed');
    });
  },
};
