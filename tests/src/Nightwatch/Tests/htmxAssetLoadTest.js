// The javascript that creates dropbuttons is not present on the /page at
// initial load.  If the once data property is added then the JS was loaded
// and triggered on the inserted content.
module.exports = {
  '@tags': ['htmx'],
  before(browser) {
    browser
      .drupalInstall({
        setupFile: `${__dirname}/../HtmxAssetLoadTestSetup.php`
      })
  },
  afterEach(browser) {
    browser.drupalLogAndEnd({ onlyOnError: true });
  },
  after(browser) {
    browser.drupalUninstall();
  },
  'Asset Load': browser => {
    browser
      .drupalRelativeURL('/htmx-test-attachments/replace')
      .waitForElementVisible('body', 1000)
      .assert.elementPresent('script[src*="dropbutton.js"]')

    browser.drupalRelativeURL('/htmx-test-attachments/page')
      .waitForElementVisible('body', 1000)
      .assert.not.elementPresent('script[src*="dropbutton.js"]')
      .waitForElementVisible('button[name="replace"]', 1000)
      .click('button[name="replace"]')
      .waitForElementVisible('div.dropbutton-wrapper', 60000)
      .waitForElementVisible('div[data-once="dropbutton"]', 60000)
      .assert.elementPresent('script[src*="dropbutton.js"]');
  },
};
