module.exports = {
  '@tags': ['htmx'],
  before(browser) {
    browser
      .drupalInstall({
        setupFile: `${__dirname}/../HtmxBlockPlacementTestSetup.php`
      })
  },
  afterEach(browser) {
    browser.drupalLogAndEnd({ onlyOnError: false });
  },
  after(browser) {
    browser.drupalUninstall();
  },
  'Dynamic Block': browser => {
    browser
      .drupalRelativeURL('/htmx-test-attachments/page')
      .waitForElementVisible('body', 1000)
      .waitForElementVisible('#block-olivero-htmx-demo', 1000)
      .waitForElementVisible('button.button', 1000)
      .click('button.button')
      .waitForElementVisible('#block-htmx-test-block', 60000)
      .assert.textContains('#block-htmx-test-block', 'Inserted by HTMX!');
  },
};
