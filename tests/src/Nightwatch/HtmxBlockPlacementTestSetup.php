<?php

declare(strict_types=1);

namespace Drupal\Tests\htmx;

use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Extension\ThemeInstallerInterface;
use Drupal\TestSite\TestSetupInterface;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Setup file used by tests/src/Nightwatch/Tests/htmxBlockPlacementTest.js.
 *
 * @see \Drupal\Tests\Scripts\TestSiteApplicationTest
 */
class HtmxBlockPlacementTestSetup implements TestSetupInterface {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  public function setup() {

    // Install Olivero and set it as the default theme.
    $theme_installer = \Drupal::service('theme_installer');
    assert($theme_installer instanceof ThemeInstallerInterface);
    $theme_installer->install(['olivero'], TRUE);
    $system_theme_config = \Drupal::configFactory()->getEditable('system.theme');
    $system_theme_config->set('default', 'olivero')->save();

    // Install required modules.
    $module_installer = \Drupal::service('module_installer');
    assert($module_installer instanceof ModuleInstallerInterface);
    $module_installer->install(['block']);
    $module_installer->install(['htmx']);
    $module_installer->install(['test_htmx_block']);
    $module_installer->install(['htmx_test_attachments']);

    // Insure correct permissions.
    $this->grantPermissions(Role::load(RoleInterface::ANONYMOUS_ID), ['access content']);
  }

}
