<?php

declare(strict_types=1);

namespace Drupal\Tests\htmx\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\htmx\Controller\HtmxEntityViewController;
use Drupal\node\Entity\Node;
use Drupal\node\NodeViewBuilder;

/**
 * Test description.
 *
 * @group htmx
 */
final class HtmxEntityViewTest extends KernelTestBase {

  use NodeCreationTrait;

  /**
   * The class under test.
   */
  protected HtmxEntityViewController $controller;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'htmx',
    'node',
    'datetime',
    'user',
    'system',
    'filter',
    'field',
    'text',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig('filter');
    $this->installConfig('node');
    $this->installSchema('node', 'node_access');
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->controller = HtmxEntityViewController::create($this->container);
  }

  /**
   * Verify the render array has the appropriate properties.
   */
  public function testRenderArray(): void {
    $node = $this->createNode(['type' => 'page']);
    $renderArray = $this->controller->view('node', $node, 'teaser');
    $this->assertArrayHasKey('#node', $renderArray);
    $this->assertInstanceOf(Node::class, $renderArray['#node']);
    $this->assertEquals($node->id(), $renderArray['#node']->id());
    $this->assertArrayHasKey('#view_mode', $renderArray);
    $this->assertEquals('teaser', $renderArray['#view_mode']);
    $this->assertArrayHasKey('#theme', $renderArray);
    $this->assertEquals('node', $renderArray['#theme']);
    $this->assertArrayHasKey('#pre_render', $renderArray);
    [$callbackObject, $method] = reset($renderArray['#pre_render']);
    $this->assertInstanceOf(NodeViewBuilder::class, $callbackObject);
    $this->assertEquals('build', $method);
  }

}
