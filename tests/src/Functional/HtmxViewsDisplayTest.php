<?php

declare(strict_types=1);

namespace Drupal\Tests\htmx\Functional;

use Drupal\Tests\views\Functional\ViewTestBase;
use Drupal\views\Views;

/**
 * Test that our views display plugin displays a view in SimplePageVariant.
 *
 * @group htmx
 */
final class HtmxViewsDisplayTest extends ViewTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'block_test',
    'htmx',
    'htmx_test_views',
  ];

  /**
   * Views used by this test.
   *
   * @var array
   */
  public static $testViews = ['test_htmx_display'];

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE, $modules = ['htmx_test_views']): void {
    parent::setUp($import_test_views, $modules);
    $this->enableViewsTestModule();

    // Enable the test_html block, to test HTML ID and attributes.
    \Drupal::state()->set('block_test.content', $this->randomMachineName());
    $this->drupalPlaceBlock('test_html', ['id' => 'test_htmx_block']);
  }

  /**
   * Test callback.
   */
  public function testDisplay(): void {
    $view = Views::getView('test_htmx_display');

    // Verify that views works as a regular page.
    $view->setDisplay('page');
    $this->drupalGet('test_page_display');
    $session = $this->assertSession();
    $session->elementExists('css', 'div.views-element-container');
    // Title is displayed NOT above the view in this page variant.
    $session->elementNotExists('css', 'main h1');
    // Verify that the test block is present using the same approach as
    // BlockHtmlTest.
    $session->elementExists('xpath', '//div[@id="block-test-htmx-block"]');

    // Verify that views works on our display.
    $view->setDisplay('htmx');
    $this->drupalGet('test_page_display_htmx');
    $session->elementExists('css', 'div.views-element-container');
    // Title is displayed above the view in this page variant.
    $session->elementExists('css', 'main h1');
    // Verify that the test block is NOT present using the same approach as
    // BlockHtmlTest.
    $session->elementNotExists('xpath', '//div[@id="block-test-htmx-block"]');

  }

}
