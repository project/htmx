<?php

declare(strict_types=1);

namespace Drupal\Tests\htmx\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\User;

/**
 * Test description.
 *
 * @group htmx
 */
final class HtmxSimplePageVariantTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['htmx', 'test_htmx_route', 'block', 'system', 'system_test'];

  /**
   * A standard user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected User $webUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Set a site slogan.
    $this->config('system.site')
      ->set('slogan', 'Community plumbing')
      ->save();
    // Add the system branding block to the page.
    $this->drupalPlaceBlock('system_branding_block', ['region' => 'header', 'id' => 'site_branding']);

    $this->webUser = $this->drupalCreateUser(['access content']);

  }

  /**
   * Test that simple page variant is selected for htmx routes..
   */
  public function testVariant(): void {
    $site_logo_xpath = '//div[@id="block-site-branding"]/a/img';
    $site_name_xpath = '//div[@id="block-site-branding"]/a[text() = "Drupal"]';
    $site_slogan_xpath = '//div[@id="block-site-branding"]/descendant::text()[last()]';
    $this->drupalLogin($this->webUser);
    // Verify setup by checking for blocks on the home page.
    $this->drupalGet('<front>');
    // Test that all branding elements are displayed.
    $this->assertSession()->elementExists('xpath', $site_logo_xpath);
    $this->assertSession()->elementExists('xpath', $site_name_xpath);
    $this->assertSession()->elementExists('xpath', $site_slogan_xpath);
    $this->assertSession()->elementTextContains('xpath', $site_slogan_xpath, 'Community plumbing');
    // Verify no block content on the htmx route (simple_page).
    $this->drupalGet('htmx-test-route/demo');
    // Test that all branding elements are NOT displayed.
    $this->assertSession()->elementNotExists('xpath', $site_logo_xpath);
    $this->assertSession()->elementNotExists('xpath', $site_name_xpath);
    $this->assertSession()->elementNotExists('xpath', $site_slogan_xpath);
    $this->assertSession()->pageTextNotContains('Community plumbing');
    $this->assertSession()->pageTextContains('Content to test main content fallback');
  }

}
