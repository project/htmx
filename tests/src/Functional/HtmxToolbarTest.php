<?php

declare(strict_types=1);

namespace Drupal\Tests\htmx\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\User;

/**
 * Test description.
 *
 * @group htmx
 */
final class HtmxToolbarTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['htmx', 'test_htmx_route', 'block', 'system', 'system_test', 'toolbar'];

  /**
   * A standard user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected User $webUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->webUser = $this->drupalCreateUser(['access content', 'access toolbar']);

  }

  /**
   * Test that simple page variant is selected for htmx routes..
   */
  public function testVariant(): void {
    $this->drupalLogin($this->webUser);
    // Verify toolbar is present.
    $this->drupalGet('/htmx-test-route/demo');
    $this->assertSession()->elementExists('xpath', '//nav[@id="toolbar-bar"]');
    // Verify toolbar is not present. drupalGet does not support our header.
    $client = $this->getHttpClient();
    $options = [
      'headers' => [
        'HX-Request' => 'true',
      ],
    ];
    $response = $client->request('GET', $this->buildUrl('/htmx-test-route/demo'), $options);
    $this->assertStringNotContainsString('<nav id="toolbar-bar"', $response->getBody()->getContents());
  }

}
