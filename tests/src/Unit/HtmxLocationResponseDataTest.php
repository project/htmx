<?php

declare(strict_types=1);

namespace Drupal\Tests\htmx\Unit;

use Drupal\Core\Url;
use Drupal\Tests\UnitTestCase;
use Drupal\htmx\Http\HtmxLocationResponseData;

/**
 * Test the location response data object.
 *
 * @group htmx
 *
 * @coversDefaultClass \Drupal\htmx\Http\HtmxLocationResponseData
 */
final class HtmxLocationResponseDataTest extends UnitTestCase {

  /**
   * The single required element in the data.
   */
  protected Url $url;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $url = $this->getMockBuilder(Url::class)
      ->disableOriginalConstructor()
      ->onlyMethods(['toString'])
      ->getMock();
    $url->expects($this->once())
      ->method('toString')
      ->willReturn('/test/path');
    $this->url = $url;
  }

  /**
   * Tests filtering empty data.
   *
   * @covers ::__toString
   */
  public function testPartialData(): void {
    $data = new HtmxLocationResponseData(
      path: $this->url,
      swap: 'swap-value'
    );
    $string = (string) $data;
    $this->assertSame('{"path":"\/test\/path","swap":"swap-value"}', $string);
  }

  /**
   * Tests complete data.
   *
   * @covers ::__toString
   */
  public function testCompleteData(): void {
    $data = new HtmxLocationResponseData(
      path: $this->url,
      source: 'source-value',
      event: 'event-value',
      headers: ['Header-one' => 'one'],
      handler: 'handler-value',
      target: 'target-value',
      swap: 'swap-value',
      select: 'select-value',
      values: ['one' => '1', 'two' => '2'],
    );
    $string = (string) $data;
    $this->assertSame('{"path":"\/test\/path","source":"source-value","event":"event-value","headers":{"Header-one":"one"},"handler":"handler-value","target":"target-value","swap":"swap-value","select":"select-value","values":{"one":"1","two":"2"}}', $string);
  }

}
