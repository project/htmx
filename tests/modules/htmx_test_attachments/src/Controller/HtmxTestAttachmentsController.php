<?php

declare(strict_types=1);

namespace Drupal\htmx_test_attachments\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\htmx\Template\HtmxAttribute;

/**
 * Returns responses for HTMX Test Attachments routes.
 */
final class HtmxTestAttachmentsController extends ControllerBase {

  /**
   * Builds the response.
   *
   * @return mixed[]
   *   A render array.
   */
  public function page(): array {
    $htmx = new HtmxAttribute(['class' => ['button'], 'name' => 'replace']);
    $htmx->get(Url::fromRoute('htmx_test_attachments.replace'))
      ->select('div.dropbutton-wrapper')
      ->swap('afterend');
    $build['content'] = [
      '#type' => 'container',
      '#attached' => [
        'library' => [
          'htmx/drupal',
        ],
      ],
      'button' => [
        '#type' => 'inline_template',
        '#template' => '<button {{ attributes }}>Click this</button>',
        '#context' => [
          'attributes' => $htmx,
        ],
      ],
    ];

    return $build;
  }

  /**
   * Builds the HTMX response.
   *
   * @return mixed[]
   *   A render array.
   */
  public function replace(): array {
    $build['content'] = [
      '#type' => 'dropbutton',
      '#dropbutton_type' => 'small',
      '#links' => [
        'simple_form' => [
          'title' => $this->t('Sample link 1'),
          'url' => Url::fromRoute('system.timezone'),
        ],
        'demo' => [
          'title' => $this->t('Sample link 2'),
          'url' => Url::fromRoute('<front>'),
        ],
      ],
    ];

    return $build;
  }

}
