<?php

declare(strict_types=1);

namespace Drupal\test_htmx_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block to insert block.
 *
 * @Block(
 *   id = "htmx_test_insert",
 *   admin_label = @Translation("Block to Insert"),
 *   category = @Translation("Test"),
 * )
 */
final class HtmxTestInsertBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build['content'] = [
      '#markup' => $this->t('Inserted by HTMX!'),
    ];
    return $build;
  }

}
