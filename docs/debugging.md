---
hide:
- toc
---

# Debugging

## Targeted approach

If you wish to see what HTMX is doing on a particular element, attach the
`htmx/debug` library in your template or render array and add
`data-hx-ext="debug"` to the element you wish to examine. This enables the
[debug extension](https://github.com/bigskysoftware/htmx-extensions/blob/main/src/debug/README.md). HTMX will log each
action to console.


## Global approach

Enable the `htmx_debug` module.  This will replace the standard minified
javascript with a standard javascript file so that with Drupal aggregation
disabled you can follow logic within HTMX.  It will also enable the HTMX debug
extension for all body content.
