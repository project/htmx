---
hide:
- toc
---
# Views

We provide an addition display for Views, _HTMX_.  This display is configured
to appear on a path in the same way as the _Page_ display.  The _HTMX_ display
uses the [SimplePageVariant](https://git.drupalcode.org/project/drupal/-/blob/11.x/core/lib/Drupal/Core/Render/Plugin/DisplayVariant/SimplePageVariant.php) described in our **Routes** documentation

Any exposed form used in an _HTMX_ display will be altered to work via HTMX.

If you need a pager with your view, an _HTMX_ pager is also
provided which loads the next page via HTMX and scrolls to the top of the view.
