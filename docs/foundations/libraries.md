---
hide:
- toc
---
# Libraries

This module provides the following Drupal asset libraries:

- `htmx/htmx` Just the upstream HTMX minified library.
- `htmx/drupal` The `htmx/htmx` plus JS to attach returned drupal assets and
  drupalBehaviors.
- `htmx/debug` The HTMX debug extension.  Add an `data-hx-ext="debug"` attribute to
  the element processing or an ancestor to see debug output in the console.
- `htmx/dialog.off-canvas` The core styles for the off-canvas dialog with some
  customization for use via htmx.
