---
hide:
- toc
---
# Entity Display

We provide the `htmx.htmx_entity_view` route at
`/htmx/{entityType}/{entity}/{viewMode}` which renders and returns the entity in
the given view mode for you to use as an endpoint when building with HTMX.

`entityType`: the entity type id
`entity`: the entity id
`viewMode`: the id of the desired view mode

Example: `/htmx/node/2/teaser`
