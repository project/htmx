---
hide:
- toc
---
# Routes

When creating your own routes to deliver responses that will only be used to
service HTMX requests, you should add `_htmx_route: 'true'` to the route
definition. This will cause Drupal to return the simpler response described
in the following section.

```yaml
your_module.custom_route:
  path: '/your_module/path'
  defaults:
    _controller: '\Drupal\your_module\Controller\CustomController::method'
    _title: 'HTMX markup'
  requirements:
    _permission: 'access content'
  options:
    _htmx_route: true
```
# Response

For performance and simplicity we make use of the core page rendering variant
[Drupal\Core\Render\Plugin\DisplayVariant\SimplePageVariant](https://git.drupalcode.org/project/drupal/-/blob/11.x/core/lib/Drupal/Core/Render/Plugin/DisplayVariant/SimplePageVariant.php). This
variant does not render any blocks, but only system messages and the main
content.

To further simplify and speed up our responses, we suppress the rendering of
the admin toolbar on any request initiated by HTMX.  To facilitate any desired
HTMX operations on the toolbar itself, we provide the `htmx.toolbar_only` route
which only renders the toolbar.
