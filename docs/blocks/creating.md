---
hide:
- toc
---
# Creating HTMX Blocks

You will find an tab added to the Block layout admin section at
`/admin/structure/htmx-block`.

Here you can add and configure blocks as you normally would.  The only
difference from configuring core blocks is that these blocks do not have a
region.
