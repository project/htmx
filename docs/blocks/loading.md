---
hide:
- toc
---
# Displaying HTMX Blocks

## Using a Block

The HTMX module offers a standard core block, _HTMX Loader_.  This block is
configured to react to an event.  When triggered, the markup for an [HTMX Block](creating.md)
is obtained via a GET request. This markup replaces the _HTMX Loader_ which was
triggered.

## In your custom markup

You can use a similar approach to that in _HTMX Loader_ in your own code. Here
is the relevant part of the render array:

```php
$htmx = new HtmxAttribute();
$htmx->get(Url::fromRoute('htmx_blocks.view', ['block' => $htmxBlockId]]))
  ->select('#block-' . Html::getId($htmxBlockId))
  ->swap('outerHTML');

$build = [
   '#attributes' => $htmx->toArray(),
]
```

There are a variety of strategies available for [hx-swap](https://htmx.org/attributes/hx-swap/).
