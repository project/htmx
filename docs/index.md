---
hide:
  - toc
---

# HTMX: tools for Drupal developers

The HTMX Module provides tools to use HTMX to build dynamic
content in Drupal using the core architecture of the web, hypermedia. This
documentation is divided into three sections. If
you are browsing these in the repository, see the named directories adjacent to
this file.  On the web the sections are linked in the left sidebar.

## Foundations

The solutions and services that make these integrations possible.

## Blocks

HTMX blocks can be configured with all the display conditions of regular blocks.
These blocks are then dynamically loaded via HTMX by configuring and placing
an HTMX Loader Block in a theme's block placement tab.

## Views

Plugins which allow building views intended to be displayed via HTMX.

## Examples

Here you find detailed discussions of examples showing how to build in Drupal
using HTMX. Examples are welcomed! Please
[open an issue for your example](https://www.drupal.org/project/issues/htmx?text=&status=All&priorities=All&categories=2&version=All&component=Example)
and attach your markdown file or make an merge request.

## More on HTMX

### Get involved with HTMX in Drupal

Bring your questions and your ideas for improving this module to our issue
queue!

If you would like to help us replace jQuery Ajax with HTMX in Drupal core, come
collaborate in these issues:

* [[POC] Implementing some components of the Ajax system using HTMX](https://www.drupal.org/project/drupal/issues/3446642)
* [[Plan] Gradually replace Drupal's AJAX system with HTMX](https://www.drupal.org/project/drupal/issues/3404409)

### Other references

* [Docs for the HTMX project](https://htmx.org/docs/)
* [Hypermedia Systems](https://hypermedia.systems/)
