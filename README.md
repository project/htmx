# For developers

## Installation

```
composer require drupal/htmx
```
## Documentation

Since this module is a tool for developers, we have chosen to include module
documentation here in the module's `docs` directory. This documentation is also
available on the web at [HTMX: tools for Drupal developers](https://project.pages.drupalcode.org/htmx/)

