#HTMX: Full Source

We provide here the current source (not minified) of HTMX as a replacement
for the minified version normally deployed.  The intention is to make it easier
to see and understand what is happening during debugging.

## SHA384 Sums

The SHA384 reduced to base64 for htmx.js version 2.0.4 is listed in
[htmx Github repository](https://github.com/bigskysoftware/htmx/blob/v2.0.4/www/content/docs.md#via-a-cdn-eg-unpkgcom)

cspell:disable
as `oeUn82QNXPuVkGCkcrInrS1twIxKhkZiFfr2TdiuObZ3n3yIeMiqcRzkIcguaof1`
cspell:enable

This sum can be [replicated and verified](https://www.srihash.org/) on the
command line:

```shell
openssl dgst -sha384 -binary js/htmx.js | openssl base64 -A
oeUn82QNXPuVkGCkcrInrS1twIxKhkZiFfr2TdiuObZ3n3yIeMiqcRzkIcguaof1
```

A manifest of SHA384 sums reduced to base64 for all the redistributed files is
given below:

cspell:disable

| Sum                                                              | File    |
|------------------------------------------------------------------|---------|
| oeUn82QNXPuVkGCkcrInrS1twIxKhkZiFfr2TdiuObZ3n3yIeMiqcRzkIcguaof1 | htmx.js |

cspell:enable


## Update htmx package instructions:

```shell
cd js/htmx
# download latest version of htmx.min.js
wget https://unpkg.com/htmx.org@2.0.n/dist/htmx.js -O htmx.js
```

or

```shell
cd js/htmx
# download latest version of htmx.min.js
curl -o htmx.js https://unpkg.com/htmx.org@2.0.n/dist/htmx.js
```
