/**
 * @file
 * Adds needed assets to html.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it in an anonymous closure.
(function (Drupal, drupalSettings, once, loadjs, htmx) {

  /**
   * Helper function to merge two objects recursively.
   *
   * @param current
   *   The object to receive the merged values.
   * @param sources
   *   The objects to merge into current.
   *
   * @see https://youmightnotneedjquery.com/#deep_extend
   */
  function mergeSettings(current, ...sources) {
    if (!current) {
      return {};
    }

    for (const obj of sources) {
      if (!obj) {
        continue;
      }

      for (const [key, value] of Object.entries(obj)) {
        switch (Object.prototype.toString.call(value)) {
          case '[object Object]':
            current[key] = current[key] || {};
            current[key] = mergeSettings(current[key], value);
            break;

          case '[object Array]':
            current[key] = mergeSettings(new Array(value.length), value);
            break;

          default:
            current[key] = value;
        }
      }
    }

    return current;
  }

  /**
   * Function to process and send the current ajax page state with each request.
   *
   * @param configRequestEvent
   *   HTMX event for request configuration.
   */
  function htmxDrupalPageState(configRequestEvent) {
    const url = new URL(configRequestEvent.detail.path, document.location.href);
    const origin = document.location.origin;
    const sameHost = origin === url.origin;
    if (sameHost) {
      // We only need to add this data for htmx requests back to the site.
      configRequestEvent.detail.headers['HX-Page-State'] = drupalSettings.ajaxPageState.libraries;
      // Swap in drupal data selectors as #id values are altered to be unique.
      configRequestEvent.detail.headers['HX-Target'] = configRequestEvent.detail.target.dataset.drupalSelector;
      configRequestEvent.detail.headers['HX-Trigger'] = configRequestEvent.detail.elt.dataset.drupalSelector;
    }
  }

  /**
   * Function to obtain, process, and attach
   * @param oobSwapEvent
   */
  function htmxDrupalAssetProcessor(oobSwapEvent) {
    // Find any inserted assets.
    const assetsTag = oobSwapEvent.detail.target.querySelector('script[data-drupal-selector="drupal-htmx-assets"]');
    if (!(assetsTag instanceof HTMLElement)) {
      return;
    }
    // Parse assets and initialize container variables.
    const assets = JSON.parse(assetsTag.textContent);
    let cssItems = [];
    let scriptItems = [];
    let scriptBottomItems = [];
    let paths = [];
    if (assets.constructor.name === 'Object') {
      // Variable assets should have properties 'styles', 'scripts,
      // 'scripts_bottom', 'settings'. See
      // HtmxResponseAttachmentsProcessor::processAssetLibraries
      cssItems = new Map(assets['styles'].map((item) => [`css!${item['#attributes']['href']}`, item]));
      scriptItems = new Map(assets['scripts'].map((item) => [item['#attributes']['src'], item]));
      scriptBottomItems = new Map(assets['scripts_bottom'].map((item) => [item['#attributes']['src'], item]));
      mergeSettings(drupalSettings, assets['settings']);
    }
    // Load CSS.
    if (cssItems instanceof Map && cssItems.size > 0) {
      // Fetch in parallel but load in sequence.
      cssItems.forEach((value, key) => paths.push(key));
      loadjs(paths, {
        async: false,
        before: function (path, linkElement) {
          const item = cssItems.get(path);
          for (const [attributeKey, attributeValue] of Object.entries(item['#attributes'])) {
            linkElement.setAttribute(attributeKey, attributeValue);
          }
        },
      });
    }
    /* By default, loadjs appends the script to the head. When scripts
     * are loaded via HTMX, their location has no impact on
     * functionality. But, since Drupal loaded scripts can choose
     * their parent element, we provide that option here for the sake of
     * consistency.
     */
    if (scriptItems instanceof Map && scriptItems.size > 0) {
      paths = [];
      scriptItems.forEach((value, key) => paths.push(key));
      // Load head JS.
      // Fetch in parallel but load in sequence.
      loadjs(paths, {
        async: false,
        before: function (path, scriptElement) {
          const item = scriptItems.get(path);
          for (const [attributeKey, attributeValue] of Object.entries(item['#attributes'])) {
            scriptElement.setAttribute(attributeKey, attributeValue);
          }
        },
      });
    }
    if (scriptBottomItems instanceof Map && scriptBottomItems.size > 0) {
      paths = [];
      scriptBottomItems.forEach((value, key) => paths.push(key));
      // Fetch in parallel but load in sequence.
      loadjs(paths, {
        async: false,
        before: function (path, scriptElement) {
          const item = scriptBottomItems.get(path);
          for (const [attributeKey, attributeValue] of Object.entries(item['#attributes'])) {
            scriptElement.setAttribute(attributeKey, attributeValue);
          }
          document.body.appendChild(scriptElement);
          // Return false to bypass loadjs' default DOM insertion
          // mechanism.
          return false;
        },
      });
    }
    // Any assets now processed.  Remove the found asset tag.
    htmx.remove(assetsTag);
  }

  /* Initialize listeners. */

  Drupal.behaviors.htmxAssets = {
    attach: () => {
      if (!once('htmxAssets', 'html').length) {
        return;
      }
      window.addEventListener('htmx:configRequest', htmxDrupalPageState);
      window.addEventListener('htmx:oobAfterSwap', htmxDrupalAssetProcessor);
    }
  };

})(Drupal, drupalSettings, once, loadjs, htmx);
