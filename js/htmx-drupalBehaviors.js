/**
 * @file
 * Connect Drupal.behaviors to htmx inserted content.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it in an anonymous closure.
(function (Drupal, once, htmx, drupalSettings) {

  function htmxDrupalBehaviors(htmxLoadEvent) {
    // The attachBehaviors method searches within the context.
    // We need to go up one level so that the loaded content is processed
    // completely.
    Drupal.attachBehaviors(htmxLoadEvent.detail.elt?.parentElement, drupalSettings);
  }

  /* Initialize listeners. */

  Drupal.behaviors.htmxBehaviors = {
    attach: () => {
      if (!once('htmxBehaviors', 'html').length) {
        return;
      }
      window.addEventListener('htmx:load', htmxDrupalBehaviors);
      window.addEventListener('htmx:oobAfterSwap', htmxDrupalBehaviors);
    }
  };

  Drupal.behaviors.htmxProcess = {
    attach: function (context) {
      if (typeof htmx === "object" && htmx.hasOwnProperty('process')) {
        htmx.process(context);
      }
    }
  };

})(Drupal, once, htmx, drupalSettings);
