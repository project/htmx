# HTMX: A Third-Party Asset to Drupal

## Introduction

We provide SHA verified copies of the HTMX assets because:

1. JS loaded from a CDN is less secure.
2. A separate install step is more cumbersome.
3. HTMX is released on a very permissive license that allows such distribution.

## Compliance

### Provenance

Authors: HTMX is collaboratively built by over 300 contributors. The project
lead is [Carson Gross](https://bigsky.software/cv/).

Copyright: The copyright notice was removed in a commit to the HTMX project
on January 15, 2024.

### License

The HTMX project changed licensing from the [FreeBSD](https://www.gnu.org/licenses/license-list.html#FreeBSD)
license to a BSD Zero Clause License, which is a [public domain equivalent](https://pitt.libguides.com/openlicensing/BSD)
license. The license provides all [four freedoms](https://www.gnu.org/philosophy/free-sw.html#four-freedoms)
without restriction. A copy of the [license](https://github.com/bigskysoftware/htmx/blob/master/LICENSE)
from the source project repo is also redistributed here with the third party
files.

### Source

The HTMX project collaborates and distributes via [https://github.com/bigskysoftware/htmx](https://github.com/bigskysoftware/htmx)

## SHA384 Sums

The SHA384 reduced to base64 for htmx.min.js version 2.0.4 is listed in the
[htmx Github repository](https://github.com/bigskysoftware/htmx/blob/v2.0.4/www/content/docs.md#via-a-cdn-eg-unpkgcom)

cspell:disable
as `HGfztofotfshcF7+8n44JQL2oJmowVChPTg48S+jvZoztPfvwD79OC/LTtG6dMp+`
cspell:enable

This sum can be [replicated and verified](https://www.srihash.org/) on the
command line:

```shell
openssl dgst -sha384 -binary js/htmx/htmx.min.js | openssl base64 -A
HGfztofotfshcF7+8n44JQL2oJmowVChPTg48S+jvZoztPfvwD79OC/LTtG6dMp+
```

A manifest of SHA384 sums reduced to base64 for all the redistributed files is
given below:

cspell:disable

| Sum                                                              | File        |
|------------------------------------------------------------------|-------------|
| HGfztofotfshcF7+8n44JQL2oJmowVChPTg48S+jvZoztPfvwD79OC/LTtG6dMp+ | htmx.min.js |
| RNXAU1D9now8hhHW/KcRwHAFwc76r0Mne6FNQvkvrznHaeJpW+Qd1S5nBRoCavje | debug.js    |

cspell:enable


## Update htmx package instructions:

```shell
cd js/htmx
# download latest version of htmx.min.js
wget https://unpkg.com/htmx.org@2.0.n/dist/htmx.min.js -O htmx.min.js
```

or

```shell
cd js/htmx
# download latest version of htmx.min.js
curl -o htmx.min.js https://unpkg.com/htmx.org@2.0.n/dist/htmx.min.js
```

or browse to [htmx releases](https://github.com/bigskysoftware/htmx/releases)
and download `htmx.min.js`

Then copy, compare and update the the debug extension with our copy.  This
extension is not minified.  The current version is at:

[https://github.com/bigskysoftware/htmx-extensions/tree/main/src/debug](https://github.com/bigskysoftware/htmx-extensions/tree/main/src/debug)
