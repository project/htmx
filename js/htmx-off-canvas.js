/**
 * This JS provides small self documenting behaviors for managing
 * off canvas dialogs.
 */

/**
 * Opens the dialog and focuses on the first input.
 *
 * @param element
 *   The dialog element.
 */
const revealOffCanvasWithInput = (element) => {
  element.show();
  htmx.find(element, "input").focus();
};

/**
 * Opens the dialog and removes the autofocus on the close button.
 *
 * @param element
 *   The dialog element.
 */
const revealOffCanvasDialog = (element) => {
  element.show();
  htmx.find(element, "button.ui-dialog-titlebar-close").blur();
};

/**
 * Removes the nearest <dialog> in the ancestry of the triggering element.
 *
 * @param element
 *   The dialog element.
 */
const removeOffCanvasDialog = (element) => {
  htmx.remove(htmx.closest(element, 'dialog'));
  htmx.trigger(htmx.find('button[name="open-plugin-dialog"]'), 'pluginDialogRemoved');
};

const setListWithDialogOpen = (element) => {
  element.setAttribute('disabled', 'disabled');
  htmx.addClass('body', 'htmx-block--library-loaded');
};

const setListWithoutDialog = (element) => {
  htmx.removeClass('body', 'htmx-block--library-loaded');
  element.removeAttribute('disabled');
};
