<?php

declare(strict_types=1);

namespace Drupal\htmx\Controller;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Url;
use Drupal\htmx\Entity\HtmxBlock;
use Drupal\htmx\Plugin\Block\HtmxLoaderBlock;
use Drupal\htmx\Template\HtmxAttribute;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for blocks admin routes.
 *
 * @see \Drupal\block\Controller\BlockLibraryController
 */
final class HtmxBlockAdminController extends ControllerBase {

  public function __construct(
    private readonly BlockManagerInterface $blockManager,
    private readonly ContextRepositoryInterface $contextRepository,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('plugin.manager.block'),
      $container->get('context.repository'),
    );
  }

  /**
   * Build a render array for the list of blocks.
   *
   * @return mixed[]
   *   The render array.
   */
  public function listBlocksDialog(): array {
    $headers = [
      ['data' => $this->t('Block')],
      ['data' => $this->t('Category')],
      ['data' => $this->t('Operations')],
    ];

    // Block admin lists blocks which work without any available context.
    $definitions = $this->blockManager->getFilteredDefinitions('htmx_blocks', $this->contextRepository->getAvailableContexts());
    // Order by category, and then by admin label.
    $definitions = $this->blockManager->getSortedDefinitions($definitions);
    // Filter out definitions that are not intended to be placed by the UI.
    $definitions = array_filter($definitions, function (array $definition) {
      return empty($definition['_block_ui_hidden']) && $definition['class'] !== HtmxLoaderBlock::class;
    });

    $rows = [];
    foreach ($definitions as $plugin_id => $plugin_definition) {
      $row = [];
      $row['title']['data'] = [
        '#type' => 'inline_template',
        '#template' => '<div class="block-filter-text-source">{{ label }}</div>',
        '#context' => [
          'label' => $plugin_definition['admin_label'],
        ],
      ];
      $row['category']['data'] = $plugin_definition['category'];
      $row['operations']['data'] = [
        '#type' => 'button',
        '#value' => 'Add',
        '#attributes' => [
          'class' => ['button'],
        ],
      ];

      $htmxAddButton = new HtmxAttribute($row['operations']['data']['#attributes']);
      $htmxAddButton->get(Url::fromRoute('entity.htmx_block.add_form', ['plugin_id' => $plugin_id]))
        ->target('#drupal-off-canvas-wrapper')
        ->select('#drupal-off-canvas-wrapper')
        ->swap('outerHTML');
      $row['operations']['data']['#attributes'] = $htmxAddButton->toArray();
      $rows[] = $row;
    }

    $build['heading'] = $this->buildHeading('select');

    $build['filter'] = [
      '#type' => 'search',
      '#title' => $this->t('Filter'),
      '#title_display' => 'invisible',
      '#size' => 30,
      '#placeholder' => $this->t('Filter by block name'),
      '#attributes' => [
        'class' => ['block-filter-text'],
        'data-element' => '.block-add-table',
        'title' => $this->t('Enter a part of the block name to filter by.'),
      ],
    ];

    $build['blocks'] = [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => $this->t('No blocks available.'),
      '#attributes' => [
        'class' => ['block-add-table'],
      ],
    ];

    $htmxDialog = new HtmxAttribute();
    $htmxDialog->on('::load', 'revealOffCanvasWithInput(this)');
    return [
      '#type' => 'inline_template',
      '#template' => '<dialog id="drupal-off-canvas-wrapper" {{ htmxAttribute }} class="htmx-dialog-off-canvas">{{ content }}</dialog>',
      '#context' => [
        'htmxAttribute' => $htmxDialog,
        'content' => $build,
      ],
    ];
  }

  /**
   * Build the block instance add form.
   *
   * @param string $plugin_id
   *   The plugin ID for the new block instance.
   *
   * @return mixed[]
   *   The block instance add form.
   */
  public function blockAddForm(string $plugin_id): array {
    // Create an HTMX block entity.
    $entity = $this->entityTypeManager()->getStorage('htmx_block')->create(['plugin' => $plugin_id]);

    $build = [];
    $build['heading'] = $this->buildHeading('add');
    $build['form'] = $this->entityFormBuilder()->getForm($entity, 'add');

    $htmxDialog = new HtmxAttribute();
    $htmxDialog->on('::load', 'revealOffCanvasDialog(this)');
    return [
      '#type' => 'inline_template',
      '#template' => '<dialog id="drupal-off-canvas-wrapper" {{ htmxAttribute }} class="htmx-dialog-off-canvas">{{ content }}</dialog>',
      '#context' => [
        'htmxAttribute' => $htmxDialog,
        'content' => $build,
      ],
    ];
  }

  /**
   * Build the block instance edit form.
   *
   * @param string $htmx_block
   *   The ID of the block instance.
   *
   * @return mixed[]
   *   The block instance edit form.
   */
  public function blockEditForm(string $htmx_block): array {
    // Load an HTMX block entity.
    $entity = $this->entityTypeManager()->getStorage('htmx_block')->load($htmx_block);

    $build = [];
    $build['heading'] = $this->buildHeading('edit');
    $build['form'] = $this->entityFormBuilder()->getForm($entity, 'edit');

    $htmxDialog = new HtmxAttribute();
    $htmxDialog->on('::load', 'revealOffCanvasDialog(this)');
    return [
      '#type' => 'inline_template',
      '#template' => '<dialog id="drupal-off-canvas-wrapper" {{ htmxAttribute }} class="htmx-dialog-off-canvas">{{ content }}</dialog>',
      '#context' => [
        'htmxAttribute' => $htmxDialog,
        'content' => $build,
      ],
    ];
  }

  /**
   * Build the block instance delete form.
   *
   * @param string $htmx_block
   *   The ID of the block instance.
   *
   * @return mixed[]
   *   The block instance delete form.
   */
  public function blockDeleteForm(string $htmx_block): array {
    // Load an HTMX block entity.
    $entity = $this->entityTypeManager()->getStorage('htmx_block')->load($htmx_block);

    $build = [];
    $build['heading'] = $this->buildHeading('delete');
    $build['form'] = $this->entityFormBuilder()->getForm($entity, 'delete');

    $htmxDialog = new HtmxAttribute();
    $htmxDialog->on('::load', 'revealOffCanvasDialog(this)');
    return [
      '#type' => 'inline_template',
      '#template' => '<dialog id="drupal-off-canvas-wrapper" {{ htmxAttribute }} class="htmx-dialog-off-canvas">{{ content }}</dialog>',
      '#context' => [
        'htmxAttribute' => $htmxDialog,
        'content' => $build,
      ],
    ];
  }

  /**
   * Returns html needed by HTMX to update the admin page.
   *
   * @return mixed[]
   *   The render array.
   */
  public function updated(): array {
    return [
      'block_list' => $this->entityTypeManager()->getListBuilder('htmx_block')->render(),
      'dialog' => $this->listBlocksDialog(),
    ];
  }

  /**
   * Build a render array for the heading.
   *
   * @param string $mode
   *   The mode of the dialog.
   *
   * @return mixed[]
   *   The render array.
   */
  protected function buildHeading(string $mode = 'edit'): array {
    // Adjust the title.
    $title = match($mode) {
      'select' => $this->t('Add an HTMX block'),
      'delete' => $this->t('Delete an HTMX block'),
      default => $this->t('Configure an HTMX block'),
    };
    // Build a refresh button for add mode.
    $htmxSelect = new HtmxAttribute([
      'class' => ['button--htmx', 'button--htmx-refresh'],
      'aria-live' => 'polite',
    ]);
    $htmxSelect->get(Url::fromRoute('htmx_blocks.htmx_system_block_library'))
      ->select('table.block-add-table')
      ->target('dialog > table.block-add-table')
      ->swap('outerHTML');
    $refresh = match($mode) {
      'select' => [
        '#type' => 'inline_template',
        '#template' => <<<'REFRESH'
<button {{ attributes }}>
        <span class="default-text">{{ add }}</span> <span class="htmx-indicator">{{ load }}</span>
</button>
REFRESH,
        '#context' => [
          'add' => $this->t('Refresh'),
          'load' => 'Loading...',
          'attributes' => $htmxSelect,
        ],
      ],
      default => [],
    };
    $htmxClose = new HtmxAttribute([
      'class' => [
        'button',
        'button--action',
        'button--primary',
        'ui-button-icon-only',
        'ui-dialog-titlebar-close',
        'button--htmx',
      ],
    ]);
    $htmxClose->on('click', 'removeOffCanvasDialog(this)');
    $heading = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'ui-dialog-titlebar',
        ],
      ],
      'title' => [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#value' => $title,
        '#attributes' => [
          'class' => [
            'ui-dialog-title',
          ],
        ],
      ],
      'refresh' => $refresh,
      'close' => [
        '#type' => 'inline_template',
        '#template' => '<button {{ attributes }} ><span class="default-text">{{ closeLabel }}</span></button>',
        '#context' => [
          'closeLabel' => $this->t('Close'),
          'attributes' => $htmxClose,
        ],
      ],
    ];
    return $heading;
  }

  /**
   * Builds the response.
   */
  public function getConfiguredBlocks(Request $request): JsonResponse {
    $matches = [];
    // Get the user input from the URL.
    $match = $request->query->get('q');

    // Verify the input before continuing.
    if (is_string($match) && strlen($match)) {
      /** @var \Drupal\htmx\Entity\HtmxBlock[] $blocks */
      $blocks = $this->entityTypeManager()
        ->getStorage('htmx_block')
        ->loadByProperties(['status' => 'true']);
      /** @var \Drupal\htmx\Entity\HtmxBlock[] $filtered */
      $filtered = array_filter($blocks, fn(HtmxBlock $block) => str_contains(strtolower($block->label()), strtolower($match)) || str_contains(strtolower($block->id()), strtolower($match)));
      foreach ($filtered as $block) {
        $matches[] = ['value' => $block->id(), 'label' => $block->label() . ' (' . $block->id() . ')'];
      }
    }
    return new JsonResponse($matches);
  }

}
