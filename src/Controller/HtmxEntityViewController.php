<?php

declare(strict_types=1);

namespace Drupal\htmx\Controller;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Utility\Error;

/**
 * Returns an entity rendered in a view mode.
 */
final class HtmxEntityViewController extends ControllerBase {

  /**
   * Builds the response.
   *
   * @param string $entityType
   *   The id of the entity type requested.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity upcast from the entity id in the request path.
   * @param string $viewMode
   *   The view mode to use in rendering the entity.
   *
   * @return mixed[]
   *   The render array.
   */
  public function view(string $entityType, EntityInterface $entity, string $viewMode = 'default'): array {
    $build = [];
    try {
      /** @var \Drupal\Core\Entity\EntityViewBuilderInterface $viewBuilder */
      $viewBuilder = $this->entityTypeManager()->getViewBuilder($entityType);
      $build = $viewBuilder->view($entity, $viewMode);
    }
    catch (InvalidPluginDefinitionException $e) {
      /*
       * \Drupal\Core\Entity\EntityTypeManager::getHandler() throws this
       * exception when it cannot get the requested view builder.
       */
      Error::logException($this->getLogger('htmx'), $e);
    }
    return $build;
  }

}
