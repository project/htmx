<?php

declare(strict_types=1);

namespace Drupal\htmx\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides autocompletion for HTMX Block configuration form.
 */
final class HtmxEventAutocomplete extends ControllerBase {

  /**
   * Builds the response.
   */
  public function getData(Request $request): JsonResponse {
    $matches = [];
    $events = $this->config('htmx.htmx_loader.autocomplete_events')->get('events');

    // Get the user input from the URL.
    $input = $request->query->get('q');

    // Verify the input before continuing.
    if (is_string($input) && strlen($input)) {
      $filtered = array_filter($events, fn($definition) => str_contains($definition['event'], $input));
      foreach ($filtered as $definition) {
        $matches[] = ['value' => $definition['event'], 'label' => $definition['event']];
      }
    }
    return new JsonResponse($matches);
  }

  /**
   * Generates a <dl> of the events.
   *
   * We cannot use #theme => 'item-list` unless we decide to use an <ul>.
   *
   * @return string[]
   *   The render array.
   */
  public static function generateDefinitionList(): array {
    $items = [];
    $events = \Drupal::config('htmx.htmx_loader.autocomplete_events')->get('events');

    foreach ($events as $definition) {
      $items[] = [
        '#type' => 'inline_template',
        '#template' => '<strong>{{ event}} </strong>: {{ description }}',
        '#context' => [
          'event' => $definition['event'],
          'description' => $definition['description'],
        ],
      ];
    }
    $build = [
      '#theme' => 'item_list',
      '#items' => $items,
    ];
    return $build;
  }

}
