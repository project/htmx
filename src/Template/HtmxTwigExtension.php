<?php

declare(strict_types=1);

namespace Drupal\htmx\Template;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig extension.
 */
final class HtmxTwigExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFunctions(): array {
    $functions[] = new TwigFunction(
      'create_htmx', [$this, 'createHtmxAttribute'],
    );
    return $functions;
  }

  /**
   * Creates an HtmxAttribute object.
   *
   * @return \Drupal\htmx\Template\HtmxAttribute
   *   Returns a new HtmxAttribute.
   */
  public function createHtmxAttribute(): HtmxAttribute {
    return new HtmxAttribute();
  }

}
