<?php

declare(strict_types=1);

namespace Drupal\htmx\Template;

use Drupal\Core\Template\Attribute;
use Drupal\Core\Template\AttributeBoolean;
use Drupal\Core\Url;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

/**
 * Adds methods for creating HTMX attributes.
 *
 * References to HTMX attributes are given on each method.
 *
 * We hope to add an interface, AttributeInterface, to core and implement that
 * rather than extend a class.  For now, this is what we can do.
 *
 * @see https://htmx.org/reference/
 */
class HtmxAttribute extends Attribute {

  /**
   * Utility function to transform camelCase strings to kebab-case strings.
   *
   * Passes kebab-case strings through without any transformation.
   *
   * @param string $identifier
   *   The string to verify or transform.
   *
   * @return string
   *   The original or transformed string.
   */
  protected function insureKebabCase(string $identifier): string {
    // Check for existing kebab case.
    $kebabParts = explode('-', $identifier);
    // If the number of lower case parts matches the number of parts, then
    // all the parts are lower case.
    $isKebab = count($kebabParts) === count(array_filter($kebabParts, function ($part) {
      return preg_match('#^[[:lower:]]+$#', $part) === 1;
    }));
    if ($isKebab) {
      return $identifier;
    }

    $converter = new CamelCaseToSnakeCaseNameConverter();
    $snakeCase = $converter->normalize($identifier);
    return preg_replace('#[_:]#', '-', $snakeCase);
  }

  /**
   * A utility method to insure the `data-hx-` prefix.
   *
   * @param string $id
   *   The HTMX attribute identifier.
   * @param string|\Drupal\htmx\Template\AttributeJson $value
   *   The attribute value.
   */
  protected function setHtmxAttribute(string $id, string|AttributeJson|AttributeBoolean $value): void {
    $this->setAttribute('data-hx-' . $id, $value);
  }

  /* ***** Request attributes ***** */

  /**
   * Issues a GET request to the specified URL.
   *
   * @param \Drupal\Core\Url $url
   *   The URL for the GET request.
   *
   * @return HtmxAttribute
   *   returns self so that attribute methods may be chained.
   *
   * @see https://htmx.org/attributes/hx-get/
   */
  public function get(Url $url): HtmxAttribute {
    $this->setHtmxAttribute('get', $url->toString());
    return $this;
  }

  /**
   * Issues a POST request to the specified URL.
   *
   * @param \Drupal\Core\Url $url
   *   The URL for the POST request.
   *
   * @return HtmxAttribute
   *   returns self so that attribute methods may be chained.
   *
   * @see https://htmx.org/attributes/hx-post/
   */
  public function post(Url $url) {
    $this->setHtmxAttribute('post', $url->toString());
    return $this;
  }

  /**
   * Issues a PUT request to the specified URL.
   *
   * @param \Drupal\Core\Url $url
   *   The URL for the PUT request.
   *
   * @return HtmxAttribute
   *   returns self so that attribute methods may be chained.
   *
   * @see https://htmx.org/attributes/hx-put/
   */
  public function put(Url $url) {
    $this->setHtmxAttribute('put', $url->toString());
    return $this;
  }

  /**
   * Issues a PATCH request to the specified URL.
   *
   * @param \Drupal\Core\Url $url
   *   The URL for the PATCH request.
   *
   * @return HtmxAttribute
   *   returns self so that attribute methods may be chained.
   *
   * @see https://htmx.org/attributes/hx-patch/
   */
  public function patch(Url $url) {
    $this->setHtmxAttribute('patch', $url->toString());
    return $this;
  }

  /**
   * Issues a DELETE request to the specified URL.
   *
   * @param \Drupal\Core\Url $url
   *   The URL for the DELETE request.
   *
   * @return HtmxAttribute
   *   returns self so that attribute methods may be chained.
   *
   * @see https://htmx.org/attributes/hx-delete/
   */
  public function delete(Url $url) {
    $this->setHtmxAttribute('delete', $url->toString());
    return $this;
  }

  /* ***** Remaining HTMX 'core' attributes ***** */

  /**
   * Handle events with inline scripts on elements.
   *
   * @param string $event
   *   An event in either camelCase or kebab-case.
   * @param string $action
   *   The action to take when the event occurs.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-on/
   */
  public function on(string $event, string $action) {
    // Special case: the `::EventName` shorthand for `htmx:EventName`.
    // Remove one leading `:` so that our final attribute is
    // `data-hx--event-name` rather than `data-hx---event-name`.
    $event = preg_replace('#^::#', ':', $event);
    $formattedEvent = 'on-' . $this->insureKebabCase($event);
    $this->setHtmxAttribute($formattedEvent, $action);
    return $this;
  }

  /**
   * Control URLs in browser history.
   *
   * Use a boolean when this attribute is added along with ::get
   * - true: pushes the fetched URL into history.
   * - false: disables pushing the fetched URL if it would otherwise be pushed
   *   due to inheritance or hx-boost.
   *
   * Use a URL to cause a push into the location bar. This may be relative or
   * absolute, as per history.pushState()
   *
   * @param bool|\Drupal\Core\Url $value
   *   Use a Url object or a boolean, depending on use case.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-push-url/
   */
  public function pushUrl(bool|Url $value) {
    if ($value instanceof Url) {
      $this->setHtmxAttribute('push-url', $value->toString());
    }
    else {
      $this->setHtmxAttribute('push-url', $value ? 'true' : 'false');
    }
    return $this;
  }

  /**
   * Select content to swap in from a response.
   *
   * Uses the selector to select elements from the response.
   *
   * @param string $selector
   *   A CSS query selector.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-select/
   */
  public function select(string $selector) {
    $this->setHtmxAttribute('select', $selector);
    return $this;
  }

  /**
   * Select content for an out-of-band swap from a response.
   *
   * Each value in the comma separated list of values can specify any valid
   * hx-swap strategy by separating the selector and the swap strategy with a
   * colon, such as #alert:afterbegin.
   *
   * @param string $selectors
   *   A comma separated list of elements to be swapped out of band.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-select-oob/
   */
  public function selectOob(string $selectors) {
    $this->setHtmxAttribute('select-oob', $selectors);
    return $this;
  }

  /**
   * Controls how content will swap in.
   *
   * The hx-swap attribute allows you to specify how the response will be
   * swapped in relative to the target of an AJAX request.
   *
   * @param string $strategy
   *   A comma separated list of elements to be swapped out of band.
   * @param bool $ignoreTitle
   *   Should `ignoreTitle:true` be appended to the strategy.  Default is true.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-swap/
   */
  public function swap(string $strategy, bool $ignoreTitle = TRUE) {
    // HTMX defaults this behavior to FALSE, that is it replaces page title.
    // We believe our most common use case is to not change the title.
    if ($ignoreTitle) {
      $strategy .= '  ignoreTitle:true';
    }
    $this->setHtmxAttribute('swap', $strategy);
    return $this;
  }

  /**
   * Designate content in a response for an out-of-band swap.
   *
   * The hx-swap-oob attribute allows you to specify that some content in a
   * response should be swapped into the DOM somewhere other than the target,
   * that is “Out of Band”. This allows you to piggyback updates to other
   * element updates on a response.
   *
   * @param string $value
   *   A comma separated list of elements to be swapped out of band.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-swap-oob/
   */
  public function swapOob(true|string $value) {
    if ($value === TRUE) {
      $this->setHtmxAttribute('swap-oob', 'true');
    }
    else {
      $this->setHtmxAttribute('swap-oob', $value);
    }
    return $this;
  }

  /**
   * Specifies the target element to receive  the incoming markup.
   *
   * The hx-target attribute allows you to target a different element for
   * swapping than the one issuing the AJAX request. There are a variety
   * of target string syntaxes.  See the URL below for details.
   *
   * @param string $target
   *   The target descriptor.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-target/
   */
  public function target(string $target) {
    $this->setHtmxAttribute('target', $target);
    return $this;
  }

  /**
   * Specifies what triggers a request.
   *
   * Used with an HTMX request attribute. Allows:
   * - An event name (e.g. “click” or “my-custom-event”) followed by an event
   *   filter and a set of event modifiers
   * - A polling definition of the form every <timing declaration>
   * - A comma-separated list of such events.
   *
   * @param string $triggerDefinition
   *   The trigger definition.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-trigger/
   */
  public function trigger(string $triggerDefinition) {
    $this->setHtmxAttribute('trigger', $triggerDefinition);
    return $this;
  }

  /**
   * Add to the parameters that will be submitted with an HTMX request.
   *
   * The value of this attribute is a list of name-expression values
   * which will be converted to JSON (JavaScript Object Notation) format.
   *
   * @param array<string, string> $values
   *   The values in an array of 'name' => 'value' pairs.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-trigger/
   */
  public function vals(array $values) {
    // The name is replaced when added in ::createAttributeValue.
    $json = new AttributeJson('json', $values);
    $this->setHtmxAttribute('vals', $json);
    return $this;
  }

  /* ***** Remaining HTMX 'core' attributes ***** */

  /**
   * Add progressive enhancement for links and forms.
   *
   * The hx-boost attribute allows you to “boost” normal anchors and form tags
   * to use AJAX instead. This has the nice fallback that, if the user does not
   * have javascript enabled, the site will continue to work.
   *
   * @param bool $value
   *   Should the element and its descendants be "boosted"?
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-boost/
   */
  public function boost(bool $value) {
    if ($value === TRUE) {
      $this->setHtmxAttribute('boost', 'true');
    }
    else {
      $this->setHtmxAttribute('boost', 'false');
    }
    return $this;
  }

  /**
   * Shows a confirm() dialog before issuing a request.
   *
   * @param string $message
   *   The user facing message.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-confirm/
   */
  public function confirm(string $message) {
    $this->setHtmxAttribute('confirm', $message);
    return $this;
  }

  /**
   * Disables HTMX processing for the given node and any descendants.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-disable/
   */
  public function disable() {
    // The name is replaced when added in ::createAttributeValue.
    $setTrue = new AttributeBoolean('disable', TRUE);
    $this->setHtmxAttribute('disable', $setTrue);
    return $this;
  }

  /**
   * Adds the disabled attribute to the specified elements during a request.
   *
   * The descriptor syntax is the same as hx-target. See the documentation
   * link below for more details.
   *
   * @param string $descriptor
   *   The attribute value.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-disabled-elt/
   */
  public function disabledElements(string $descriptor) {
    $this->setHtmxAttribute('disabled-elt', $descriptor);
    return $this;
  }

  /**
   * Control and disable automatic HTMX attribute inheritance for child nodes.
   *
   * @param string $names
   *   The attribute names to disinherit or * for all.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-disinherit/
   */
  public function disinherit(string $names) {
    $this->setHtmxAttribute('disinherit', $names);
    return $this;
  }

  /**
   * Changes the request encoding type.
   *
   * @param string $method
   *   The encoding method.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-encoding/
   */
  public function encoding(string $method = 'multipart/form-data') {
    $this->setHtmxAttribute('encoding', $method);
    return $this;
  }

  /**
   * Enables HTMX extensions for an element and descendants.
   *
   * @param string $names
   *   An extension name, or a comma separated list of names.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-ext/
   */
  public function ext(string $names) {
    $this->setHtmxAttribute('ext', $names);
    return $this;
  }

  /**
   * Add to the headers that will be submitted with an HTMX request.
   *
   * @param array<string, string> $headerValues
   *   The header values as name => value.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-headers/
   */
  public function headers(array $headerValues) {
    $json = new AttributeJson('json', $headerValues);
    $this->setHtmxAttribute('headers', $json);
    return $this;
  }

  /**
   * Prevent sensitive data being saved to the history cache.
   *
   * Set the hx-history attribute to false on any element in the current
   * document, or any html fragment loaded into the current document by htmx,
   * to prevent sensitive data being saved to the localStorage cache when htmx
   * takes a snapshot of the page state.
   *
   * @param bool $value
   *   Sets the string value to 'true' or 'false'. Defaults to FALSE.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-history/
   */
  public function history(bool $value = FALSE) {
    if ($value) {
      $this->setHtmxAttribute('history', 'true');
    }
    else {
      $this->setHtmxAttribute('history', 'false');
    }
    return $this;
  }

  /**
   * The element to snapshot and restore during history navigation.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-history-elt/
   */
  public function historyElement() {
    // The name is replaced when added in ::createAttributeValue.
    $setTrue = new AttributeBoolean('history-elt', TRUE);
    $this->setHtmxAttribute('history-elt', $setTrue);
    return $this;
  }

  /**
   * Include additional element values in HTMX requests.
   *
   * The descriptor syntax is the same as hx-target. See the documentation
   * link below for more details.
   *
   * @param string $descriptors
   *   The element descriptors.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-include/
   */
  public function include(string $descriptors) {
    $this->setHtmxAttribute('include', $descriptors);
    return $this;
  }

  /**
   * The element to put the htmx-request class on during the request.
   *
   * @param string $selector
   *   The element CSS selector value. Selector may be prefixed with `closest`.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-indicator/
   */
  public function indicator(string $selector) {
    $this->setHtmxAttribute('indicator', $selector);
    return $this;
  }

  /**
   * Control automatic attribute inheritance for child nodes.
   *
   * HTMX evaluates attribute inheritance with hx-inherit in two ways when
   * hx-inherit is set on a parent node:
   *  - data-hx-inherit="*"
   *    All attribute inheritance for this element will be enabled.
   * - data-hx-hx-inherit="hx-select hx-get hx-target"
   *   Enable inheritance for only one or multiple specified attributes.
   *
   * @param string $attributes
   *   The attributes to inherit.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-inherit/
   */
  public function inherit(string $attributes) {
    $this->setHtmxAttribute('inherit', $attributes);
    return $this;
  }

  /**
   * Filters the parameters that will be submitted with a request.
   *
   * @param string $filter
   *   The filter string. Multiple syntax options, see the link below.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-params/
   */
  public function params(string $filter) {
    $this->setHtmxAttribute('params', $filter);
    return $this;
  }

  /**
   * Specifies elements to keep unchanged between requests.
   *
   * @param string $id
   *   The id attribute of the element to preserve.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-preserve/
   */
  public function preserve(string $id) {
    $this->setHtmxAttribute('preserve', $id);
    return $this;
  }

  /**
   * Shows a prompt() before submitting a request.
   *
   * @param string $message
   *   The message to display in the prompt.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-prompt/
   */
  public function prompt(string $message) {
    $this->setHtmxAttribute('prompt', $message);
    return $this;
  }

  /**
   * Control URLs in the browser location bar.
   *
   * Use a boolean when this attribute is added along with a request:
   * - true: replaces the fetched URL in the browser navigation bar.
   * - false: disables replacing the fetched URL if it would otherwise be
   *   replaced due to inheritance.
   *
   * Use a URL to replace the value in the location bar. This may be relative or
   * absolute, as per history.replaceState().
   *
   * @param bool|\Drupal\Core\Url $value
   *   Use a Url object or a boolean, depending on use case.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-replace-url/
   */
  public function replaceUrl(bool|Url $value) {
    if ($value instanceof Url) {
      $this->setHtmxAttribute('replace-url', $value->toString());
    }
    else {
      $this->setHtmxAttribute('replace-url', $value ? 'true' : 'false');
    }
    return $this;
  }

  /**
   * Configures various aspects of the request.
   *
   * The hx-request attribute supports the following configuration values:
   * - timeout: (integer) the timeout for the request, in milliseconds.
   * - credentials: (boolean) if the request will send credentials.
   * - noHeaders: (boolean) strips all headers from the request.
   *
   * Dynamic javascript values are not supported for security and for
   * simplicity.  If you need calculated values you should do determine them
   * here on the server-side
   *
   * @param array<string, int|bool> $configValues
   *   The configuration values as name => value.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-headers/
   */
  public function request(array $configValues) {
    $json = new AttributeJson('json', $configValues);
    $this->setHtmxAttribute('request', $json);
    return $this;
  }

  /**
   * Synchronize AJAX requests between multiple elements.
   *
   * @param string $selector
   *   A CSS selector followed by a strategy.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-sync/
   */
  public function sync(string $selector) {
    $this->setHtmxAttribute('sync', $selector);
    return $this;
  }

  /**
   * Cause an element to validate itself before it submits a request.
   *
   * @param bool $value
   *   Should the element validate before the request.
   *
   * @return $this
   *
   * @see https://htmx.org/attributes/hx-validate/
   */
  public function validate(bool $value = TRUE) {
    if ($value) {
      $this->setHtmxAttribute('validate', 'true');
    }
    else {
      $this->setHtmxAttribute('validate', 'false');
    }
    return $this;
  }

}
