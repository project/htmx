<?php

declare(strict_types=1);

namespace Drupal\htmx\Template;

use Drupal\Component\Utility\Html;
use Drupal\Core\Template\AttributeValueBase;

/**
 * Represents an attribute containing JSON formatted data.
 *
 * AttributeValueBase renders attributes in `double-quoted attribute value
 * syntax` which cannot contain JSON since JSON requires its properties to be
 * double-quoted.  This implementation uses `single-quoted attribute value
 * syntax` to avoid this issue.
 *
 * @see https://html.spec.whatwg.org/#attributes-2
 */
class AttributeJson extends AttributeValueBase {

  const RENDER_EMPTY_ATTRIBUTE = FALSE;

  /**
   * Flips the attribute quotes from single to double to contain valid JSON.
   *
   * @return string
   *   The rendered attribute
   */
  public function render() {
    $value = (string) $this;
    if ($value !== '') {
      return Html::escape($this->name) . "='" . $value . "'";
    }
    // Return the empty string so that we always return a string.
    return $value;
  }

  /**
   * Encode the array as JSON.
   *
   * @return string
   *   The JSON encoded array or empty string on failure.
   */
  public function __toString() {
    $value = $this->value();
    $value = is_array($value) ? $value : [$value];
    // Escape the values.
    $data = [];
    foreach ($value as $key => $item) {
      $data[Html::escape($key)] = is_string($item) ? Html::escape($item) : $item;
    }
    $string = json_encode($data, JSON_FORCE_OBJECT | JSON_NUMERIC_CHECK);
    return $string ? $string : '';
  }

}
