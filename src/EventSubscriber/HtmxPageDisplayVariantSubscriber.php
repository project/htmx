<?php

declare(strict_types=1);

namespace Drupal\htmx\EventSubscriber;

use Drupal\Core\Render\PageDisplayVariantSelectionEvent;
use Drupal\Core\Render\RenderEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Drupal should return a simple page for any route with our route option set.
 *
 * @code
 * route.name:
 *  ...
 *  options:
 *    _htmx_route: true
 * @endcode
 */
class HtmxPageDisplayVariantSubscriber implements EventSubscriberInterface {

  /**
   * Selects the simple page display variant.
   *
   * @param \Drupal\Core\Render\PageDisplayVariantSelectionEvent $event
   *   The event to process.
   */
  public function onSelectPageDisplayVariant(PageDisplayVariantSelectionEvent $event): void {
    $routeMatch = $event->getRouteMatch();
    $htmxRoute = $routeMatch->getRouteObject()->getOption('_htmx_route') ?? FALSE;
    if ($htmxRoute) {
      $event->setPluginId('simple_page');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // We want to run after BlockPageDisplayVariantSubscriber.
    $events[RenderEvents::SELECT_PAGE_DISPLAY_VARIANT][] = ['onSelectPageDisplayVariant', -100];
    return $events;
  }

}
