<?php

declare(strict_types=1);

namespace Drupal\htmx\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\htmx\Controller\HtmxEventAutocomplete;
use Drupal\htmx\Template\HtmxAttribute;

/**
 * Provides a block to dynamically load other blocks.
 */
#[Block(
  id: "htmx_loader",
  admin_label: new TranslatableMarkup("HTMX Loader"),
  category: new TranslatableMarkup("HTMX"),
)]
final class HtmxLoaderBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'htmx_block_id' => '',
      'event' => '',
      'advanced' => FALSE,
      'filter' => '',
      'from' => '',
      'delay' => '',
      'target' => '',
      'throttle' => '',
      'consume' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form['htmx_block_id'] = [
      '#type' => 'textfield',
      '#autocomplete_route_name' => 'htmx_block.htmx_block_autocomplete',
      '#title' => $this->t('HTMX Block'),
      '#required' => TRUE,
      '#description' => $this->t('Select an <a href="@path">HTMX Block</a> to load in place of this block on the configured event.', ['@path' => '/admin/structure/htmx-block']),
      '#default_value' => $this->configuration['htmx_block_id'] ?? '',
    ];
    $form['event'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Triggering Event'),
      '#default_value' => $this->configuration['event'] ?? '',
      '#required' => TRUE,
      '#description' => $this->t('Select or input an event to use for <em>hx-trigger</em>.'),
      '#autocomplete_route_name' => 'htmx_blocks.htmx_loader_autocomplete',
    ];
    $form['advanced'] = [
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['advanced'] ?? FALSE,
      '#title' => $this->t('<em>Advanced:</em> Manually configure multiple events'),
    ];
    $form['modifiers'] = [
      '#type' => 'details',
      '#title' => $this->t('Event modifiers'),
      'filter' => [
        '#type' => 'textfield',
        '#title' => $this->t('Filter'),
        '#description' => $this->t('Events can be filtered by a boolean javascript expression. If this expression evaluates to true the event will be triggered, otherwise it will be ignored.'),
        '#default_value' => $this->configuration['filter'] ?? '',
        '#states' => [
          'disabled' => [':input[name="settings[advanced]"]' => ['checked' => TRUE]],
        ],
      ],
      'from' => [
        '#type' => 'textfield',
        '#title' => $this->t('From'),
        '#description' => $this->t('An HTMX extended CSS selector that allows the event that triggers a request to come from another element in the document (e.g. listening to a key event on the body, to support hot keys).'),
        '#default_value' => $this->configuration['from'] ?? '',
        '#states' => [
          'disabled' => [':input[name="settings[advanced]"]' => ['checked' => TRUE]],
        ],
      ],
      'extended-css' => [
        '#type' => 'details',
        '#title' => $this->t('About HTMX extended CSS selectors'),
        'htmx-css-selectors' => $this->getExtendCssMarkup(),
      ],
      'target' => [
        '#type' => 'textfield',
        '#title' => $this->t('Target'),
        '#description' => $this->t('Enter a standard CSS selector to filter on the target of the event.'),
        '#default_value' => $this->configuration['target'] ?? '',
        '#states' => [
          'disabled' => [':input[name="settings[advanced]"]' => ['checked' => TRUE]],
        ],
      ],
      'delay' => [
        '#type' => 'textfield',
        '#title' => $this->t('Delay'),
        '#description' => $this->t('Enter an integer followed by s, ms, or m for the delay that will occur before an event triggers a request. If the event is seen again it will reset the delay. Conflicts with <em>Throttle</em> modifier.'),
        '#default_value' => $this->configuration['delay'] ?? '',
        '#placeholder' => '<int>ms | <int>s | <int>m',
        '#states' => [
          'disabled' => [':input[name="settings[advanced]"]' => ['checked' => TRUE]],
        ],
      ],
      'throttle' => [
        '#type' => 'textfield',
        '#title' => $this->t('Throttle'),
        '#description' => $this->t('Enter an integer followed by s, ms, or m for the throttle that will occur before an event triggers a request. If the event is seen again before the throttle completes, it is ignored, the element will trigger at the end of the throttle. Conflicts with <em>Delay</em> modifier.'),
        '#default_value' => $this->configuration['throttle'] ?? '',
        '#placeholder' => '<int>ms | <int>s | <int>m',
        '#states' => [
          'disabled' => [':input[name="settings[advanced]"]' => ['checked' => TRUE]],
        ],
      ],
      'consume' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Consume'),
        '#description' => $this->t('The event will not trigger any other htmx requests on parents (or on elements listening on parents'),
        '#default_value' => $this->configuration['consume'] ?? FALSE,
        '#states' => [
          'disabled' => [':input[name="settings[advanced]"]' => ['checked' => TRUE]],
        ],
      ],
    ];
    $form['info'] = [
      '#type' => 'details',
      '#title' => $this->t('More information about events'),
      'description' => [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t(
          'The <a href="@htmx-event">HTMX</a> & <a href="@js-event">Javascript</a> events listed below are offered for autocompletion.  Any event name may be entered and will be used with <a href="@url"><em>hx-trigger</em></a>.',
          [
            '@htmx-event' => 'https://htmx.org/reference/#events',
            '@js-event' => 'https://html.spec.whatwg.org/multipage/indices.html#events-2',
            '@url' => 'https://htmx.org/attributes/hx-trigger/',
          ]),
      ],
      'list' => HtmxEventAutocomplete::generateDefinitionList(),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $modifiers = $form_state->getValue('modifiers');
    if (!empty($modifiers['delay']) && !empty($modifiers['throttle'])) {
      $form_state->setError($form['modifiers']['delay'], $this->t('Remove the Delay or Throttle modifier value.'));
      $form_state->setError($form['modifiers']['throttle'], $this->t('Remove the Delay or Throttle modifier value.'));
    }
    if (!$this->validTimeString($modifiers['delay'])) {
      $form_state->setError($form['modifiers']['delay'], $this->t('Delay time is not a valid interval.'));
    }
    if (!$this->validTimeString($modifiers['throttle'])) {
      $form_state->setError($form['modifiers']['throttle'], $this->t('Throttle time is not a valid interval.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    $this->configuration['htmx_block_id'] = $form_state->getValue('htmx_block_id');
    $this->configuration['event'] = $form_state->getValue('event');
    $this->configuration['advanced'] = $form_state->getValue('advanced');
    $modifiers = $form_state->getValue('modifiers');
    $this->configuration['filter'] = $modifiers['filter'];
    $this->configuration['from'] = $modifiers['from'];
    $this->configuration['delay'] = $modifiers['delay'];
    $this->configuration['target'] = $modifiers['target'];
    $this->configuration['throttle'] = $modifiers['throttle'];
    $this->configuration['consume'] = $modifiers['consume'];
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $htmx = new HtmxAttribute();
    $htmx->get(Url::fromRoute('htmx_blocks.view', ['block' => $this->configuration['htmx_block_id']]))
      ->trigger($this->assembleEventValue())
      ->select('#block-' . Html::getId($this->configuration['htmx_block_id']))
      ->swap('outerHTML');

    $build = [
      '#type' => 'container',
      '#attributes' => $htmx->toArray(),
      '#attached' => [
        'library' => [
          'htmx/drupal',
        ],
      ],
    ];
    return $build;
  }

  /**
   * Assembles a render array for the content of the extended markup detail.
   *
   * @return string[]
   *   A render array.
   */
  protected function getExtendCssMarkup(): array {
    return [
      '#type' => 'inline_template',
      '#template' => <<<SELECTORS
        <p>The extended CSS selector here allows for the following non-standard CSS values:</p>
        <ul>
          <li><code>document</code> - listen for events on the document</li>
          <li><code>window</code> - listen for events on the window</li>
          <li><code>closest <em>CSS selector</em></code> - finds the closest ancestor element or itself, matching the given css selector</li>
          <li><code>find <em>CSS selector</em></code> - finds the closest child matching the given css selector</li>
          <li><code>next</code> resolves to <code>element.nextElementSibling</code></li>
          <li><code>next <em>CSS selector</em></code> scans the DOM forward for the first element that matches the given CSS selector. (e.g. <code>next .error</code> will target the closest following sibling element with error class)</li>
          <li><code>previous</code> resolves to <code>element.previousElementSibling</code></li>
          <li><code>previous <em>CSS selector</em></code> scans the DOM backwards for the first element that matches the given CSS selector. (e.g <code>previous .error</code> will target the closest previous sibling with error class)</li>
          </ul>
SELECTORS,
    ];
  }

  /**
   * Validates an HTMX time interval.
   *
   * @param string $time
   *   The time string.
   *
   * @return bool
   *   True if valid.
   */
  protected function validTimeString(string $time): bool {
    if ($time === '' || preg_match('/^\d+(ms|m|s)$/', $time)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Assembles an event specification string for use with `hx-trigger`.
   *
   * @return string
   *   The assembled event specification.
   */
  protected function assembleEventValue(): string {
    $eventString = '';
    if ($this->configuration['event'] !== '') {
      $eventString .= $this->configuration['event'];
    }
    if (!$this->configuration['advanced']) {
      if ($this->configuration['filter'] !== '') {
        $filter = $this->configuration['filter'];
        $eventString .= "[$filter]";
      }
      if ($this->configuration['consume']) {
        $eventString .= ' consume';
      }
      if ($this->configuration['delay'] !== '') {
        $delay = $this->configuration['delay'];
        $eventString .= " delay:$delay";
      }
      elseif ($this->configuration['throttle'] !== '') {
        $throttle = $this->configuration['throttle'];
        $eventString .= " throttle:$throttle";
      }
      if ($this->configuration['target'] !== '') {
        $target = $this->configuration['target'];
        $eventString .= " target:$target";
      }
      if ($this->configuration['from'] !== '') {
        $from = $this->configuration['from'];
        $eventString .= " from:$from";
      }
      return $eventString;
    }

    return $eventString;
  }

}
