<?php

declare(strict_types=1);

namespace Drupal\htmx\Plugin\views\display;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Attribute\ViewsDisplay;
use Drupal\views\Plugin\views\display\PathPluginBase;
use Symfony\Component\Routing\Route;

/**
 * The plugin that handles a full page for HTMX.
 *
 * @see \Drupal\views\Plugin\views\display\Page.php
 *
 * @ingroup views_display_plugins
 */
#[ViewsDisplay(
  id: "htmx",
  title: new TranslatableMarkup("HTMX"),
  help: new TranslatableMarkup("Display the view as a <em>simple</em> page, with a URL for use in HTMX requests."),
  uses_route: TRUE,
  contextual_links_locations: ["view"],
  theme: "views_view",
  admin: new TranslatableMarkup("HTMX"),
)]
class Htmx extends PathPluginBase {

  /**
   * {@inheritdoc}
   */
  public static function buildBasicRenderable($view_id, $display_id, array $args = [], ?Route $route = NULL) {
    $build = parent::buildBasicRenderable($view_id, $display_id, $args);

    if ($route) {
      $build['#view_id'] = $route->getDefault('view_id');
      $build['#view_display_plugin_id'] = $route->getOption('_view_display_plugin_id');
      $build['#view_display_show_admin_links'] = $route->getOption('_view_display_show_admin_links');
    }
    else {
      throw new \BadFunctionCallException('Missing route parameters.');
    }

    return $build;
  }

  /**
   * Executes the view and returns data in the format required.
   *
   * @return array|null
   *   A renderable array containing the view output or NULL if the build
   *   process failed.
   *
   * @see \Drupal\views\Plugin\views\display\Page::execute
   */
  public function execute(): ?array {
    parent::execute();

    // And now render the view.
    $render = $this->view->render();

    if (is_array($render)) {
      $render += [
        '#title' => [
          '#markup' => $this->view->getTitle(),
          '#allowed_tags' => Xss::getHtmlTagList(),
        ],
      ];
    }
    return $render;
  }

  /**
   * {@inheritdoc}
   */
  public function optionsSummary(&$categories, &$options) {
    parent::optionsSummary($categories, $options);

    // If the display path starts with 'admin/' the page will be rendered with
    // the Administration theme regardless of the 'use_admin_theme' option
    // therefore, we need to set the summary message to reflect this.
    if (str_starts_with($this->getOption('path') ?? '', 'admin/')) {
      $admin_theme_text = $this->t('Yes (admin path)');
    }
    elseif ($this->getOption('use_admin_theme')) {
      $admin_theme_text = $this->t('Yes');
    }
    else {
      $admin_theme_text = $this->t('No');
    }

    $options['use_admin_theme'] = [
      'category' => 'page',
      'title' => $this->t('Administration theme'),
      'value' => $admin_theme_text,
      'desc' => $this->t('Use the administration theme when rendering this display.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['#title'] .= $this->t('Administration theme');
    $form['use_admin_theme'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use the administration theme'),
      '#default_value' => $this->getOption('use_admin_theme'),
    ];
    if (str_starts_with($this->getOption('path') ?? '', 'admin/')) {
      $form['use_admin_theme']['#description'] = $this->t('Paths starting with "@admin" always use the administration theme.', ['@admin' => 'admin/']);
      $form['use_admin_theme']['#default_value'] = TRUE;
      $form['use_admin_theme']['#attributes'] = ['disabled' => 'disabled'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    parent::submitOptionsForm($form, $form_state);

    if ($form_state->getValue('use_admin_theme')) {
      $this->setOption('use_admin_theme', $form_state->getValue('use_admin_theme'));
    }
    else {
      unset($this->options['use_admin_theme']);
      unset($this->display['display_options']['use_admin_theme']);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getRoute($view_id, $display_id) {
    $route = parent::getRoute($view_id, $display_id);

    // Explicitly set HTML as the format for Page displays.
    $route->setRequirement('_format', 'html');

    if ($this->getOption('use_admin_theme')) {
      $route->setOption('_admin_route', TRUE);
    }

    // This is an HTMX route.
    $route->setOption('_htmx_route', TRUE);

    return $route;
  }

  /**
   * {@inheritdoc}
   */
  public function getArgumentText() {
    return [
      'filter value not present' => $this->t('When the filter value is <em>NOT</em> in the URL'),
      'filter value present' => $this->t('When the filter value <em>IS</em> in the URL or a default is provided'),
      'description' => $this->t('The contextual filter values are provided by the URL.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getPagerText() {
    return [
      'items per page title' => $this->t('Items per page'),
      'items per page description' => $this->t('Enter 0 for no limit.'),
    ];
  }

}
