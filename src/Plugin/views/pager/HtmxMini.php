<?php

declare(strict_types=1);

namespace Drupal\htmx\Plugin\views\pager;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Attribute\ViewsPager;
use Drupal\views\Plugin\views\pager\Mini;

/**
 * The plugin to handle mini pager using HTMX.
 *
 * @ingroup views_pager_plugins
 */
#[ViewsPager(
  id: "htmx_mini",
  title: new TranslatableMarkup("HTMX mini pager"),
  short_title: new TranslatableMarkup("HTMX"),
  help: new TranslatableMarkup("A simple pager containing previous and next links that load using HTMX."),
  theme: "htmx_mini_pager",
)]
class HtmxMini extends Mini {

  /**
   * {@inheritdoc}
   */
  public function render($input) {
    $build = parent::render($input);
    $build['#view'] = $this->view;
    $build['#attached']['library'][] = 'htmx/drupal';
    return $build;
  }

}
