<?php

declare(strict_types=1);

namespace Drupal\htmx\Render;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Asset\AttachedAssetsInterface;
use Drupal\Core\Render\AttachmentsInterface;
use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\Render\HtmlResponseAttachmentsProcessor;
use Drupal\htmx\Template\HtmxAttribute;

/**
 * Prepares attachment for HTMX powered responses.
 *
 * Extends the HTML response processor encode attachment data.
 *
 * @see \Drupal\Core\EventSubscriber\HtmxResponseSubscriber
 * @see \Drupal\Core\Render\HtmlResponseAttachmentsProcessor::processAttachments
 * @see js/htmx-assetManagement.js
 */
class HtmxResponseAttachmentsProcessor extends HtmlResponseAttachmentsProcessor {

  /**
   * {@inheritdoc}
   */
  public function processAttachments(AttachmentsInterface $response): HtmlResponse {
    $processed = parent::processAttachments($response);
    if (!($processed instanceof HtmlResponse)) {
      // Something has gone wrong. We sent an HtmlResponse that also
      // implemented AttachmentInterface and should have received the same back.
      throw new \TypeError("HtmlResponseAttachmentsProcessor::processAttachments should return an HtmlResponse.");
    }
    return $processed;
  }

  /**
   * {@inheritdoc}
   */
  protected function processAssetLibraries(AttachedAssetsInterface $assets, array $placeholders) {
    $request = $this->requestStack->getCurrentRequest();
    if ($request->headers->has('HX-Page-State')) {
      $uncompressed = UrlHelper::uncompressQueryParameter($request->headers->get('HX-Page-State'));
      $libraries = explode(',', $uncompressed);
      $assets->setAlreadyLoadedLibraries($libraries);
    }
    $settings = [];
    $variables = [];
    $maintenance_mode = defined('MAINTENANCE_MODE') || \Drupal::state()->get('system.maintenance_mode');

    // Print styles - if present.
    if (isset($placeholders['styles'])) {
      // Optimize CSS if necessary, but only during normal site operation.
      $optimize_css = !$maintenance_mode && $this->config->get('css.preprocess');
      $variables['styles'] = $this->cssCollectionRenderer->render($this->assetResolver->getCssAssets($assets, $optimize_css, $this->languageManager->getCurrentLanguage()));
    }

    // Copy and adjust parent::processAssetLibraries to adjust and
    // remove drupalSettings in line with
    // AjaxResponseAttachmentsProcessor::buildAttachmentsCommands
    // Print scripts - if any are present.
    if (isset($placeholders['scripts']) || isset($placeholders['scripts_bottom'])) {
      // Optimize JS if necessary, but only during normal site operation.
      $optimize_js = !$maintenance_mode && $this->config->get('js.preprocess');
      [$js_assets_header, $js_assets_footer] = $this->assetResolver
        ->getJsAssets($assets, $optimize_js, $this->languageManager->getCurrentLanguage());
      if (isset($js_assets_header['drupalSettings'])) {
        $settings = $js_assets_header['drupalSettings']['data'];
        unset($js_assets_header['drupalSettings']);
      }
      if (isset($js_assets_footer['drupalSettings'])) {
        $settings = $js_assets_footer['drupalSettings']['data'];
        unset($js_assets_footer['drupalSettings']);
      }
      if (empty($settings)) {
        $settings = $assets->getSettings();
      }
      $variables['scripts'] = $this->jsCollectionRenderer->render($js_assets_header);
      $variables['scripts_bottom'] = $this->jsCollectionRenderer->render($js_assets_footer);
      unset($settings['path']);
      $variables['settings'] = $settings;
    }
    $htmx = new HtmxAttribute();
    $htmx->swapOob('beforeend:body');
    $assetData = [
      'styles' => [],
      'scripts' => [],
      'scripts_bottom' => [
        '#type' => 'container',
        '#attributes' => $htmx,
        'libraries' => [
          '#type' => 'html_tag',
          '#tag' => 'script',
          '#attributes' => [
            'type' => 'application/json',
            'data-drupal-selector' => 'drupal-htmx-assets',
          ],
          '#value' => Json::encode($variables),
        ],
      ],
    ];
    return $assetData;
  }

}
