<?php

declare(strict_types=1);

namespace Drupal\htmx\Render;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\Context\ContextHandlerInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\block\BlockViewBuilder;
use Drupal\htmx\Entity\HtmxBlock;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Matcher\RequestMatcherInterface;

/**
 * A service that renders HTMX blocks in the context of the requesting page.
 */
final class HtmxBlockView implements HtmxBlockViewInterface {

  public function __construct(
    private readonly ModuleHandlerInterface $moduleHandler,
    private readonly RequestMatcherInterface $routeMatchingEnhancer,
    private readonly RequestStack $requestStack,
    private readonly ContextRepositoryInterface $contextRepository,
    private readonly ContextHandlerInterface $contextHandler,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function build(HtmxBlock $block): array {
    $build = [];
    $currentRequest = $this->requestStack->getCurrentRequest();
    // Verify that this is an HTMX request.
    if (
      $currentRequest instanceof Request
      && $currentRequest->headers->has('HX-Current-URL')) {
      $simulatedContext = Request::create(
        uri: $currentRequest->headers->get('HX-Current-URL'),
        cookies: $currentRequest->cookies->all(),
        server: $currentRequest->server->all(),
      );
      // Add the simulated request to the top of the stack.
      $parameters = $this->routeMatchingEnhancer->matchRequest($simulatedContext);
      $simulatedContext->attributes->add($parameters);
      unset($parameters['_route'], $parameters['_controller']);
      $simulatedContext->attributes->set('_route_params', $parameters);
      $this->requestStack->push($simulatedContext);
      if ($block->access('view')) {
        $build = $this->renderBlock($block);
      }
      // Remove the simulated context.
      $this->requestStack->pop();
    }
    return $build;
  }

  /**
   * Renders the block.
   *
   * @param \Drupal\htmx\Entity\HtmxBlock $block
   *   The HTMX block.
   *
   * @return mixed[]
   *   The render array.
   *
   * @see BlockViewBuilder::buildPreRenderableBlock()
   *
   * @throws \Drupal\Component\Plugin\Exception\ContextException
   * @throws \Drupal\Component\Plugin\Exception\MissingValueContextException
   */
  protected function renderBlock(HtmxBlock $block) {
    $plugin = $block->getPlugin();
    $plugin_id = $plugin->getPluginId();
    $base_id = $plugin->getBaseId();
    $derivative_id = $plugin->getDerivativeId();
    $configuration = $plugin->getConfiguration();

    // Inject runtime contexts.
    if ($plugin instanceof ContextAwarePluginInterface) {
      $contexts = $this->contextRepository->getRuntimeContexts($plugin->getContextMapping());
      $this->contextHandler->applyContextMapping($plugin, $contexts);
    }

    // Create the render array for the block as a whole.
    // @see template_preprocess_block().
    $build = [
      '#theme' => 'block',
      '#attributes' => [],
      '#configuration' => $configuration,
      '#plugin_id' => $plugin_id,
      '#base_plugin_id' => $base_id,
      '#derivative_plugin_id' => $derivative_id,
      '#id' => $block->id(),
      '#pre_render' => [
        BlockViewBuilder::class . '::preRender',
      ],
      // Add the htmx_block entity which is used in the #pre_render method.
      '#block' => $block,
    ];

    // If an alter hook wants to modify the block contents, it can append
    // another #pre_render hook.
    $this->moduleHandler->alter(['block_view', "block_view_$base_id"], $build, $plugin);

    return $build;
  }

}
