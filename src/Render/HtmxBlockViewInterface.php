<?php

declare(strict_types=1);

namespace Drupal\htmx\Render;

use Drupal\htmx\Entity\HtmxBlock;

/**
 * Builds blocks for dynamic insertion into HTMX managed contexts.
 */
interface HtmxBlockViewInterface {

  /**
   * Build the requested block.
   *
   * The block is built as if it had been requested in the context of the
   * page originating the request.
   *
   * @param \Drupal\htmx\Entity\HtmxBlock $htmxBlock
   *   The block to view.
   *
   * @return mixed[]
   *   The render array.
   */
  public function build(HtmxBlock $htmxBlock): array;

}
