<?php

declare(strict_types=1);

namespace Drupal\htmx\Entity;

use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Condition\ConditionInterface;
use Drupal\Core\Condition\ConditionPluginCollection;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;

/**
 * Provides an interface defining a htmx block entity type.
 */
interface HtmxBlockInterface extends ConfigEntityInterface, EntityWithPluginCollectionInterface {

  /**
   * Returns the plugin instance.
   *
   * @return \Drupal\Core\Block\BlockPluginInterface
   *   The plugin instance for this block.
   */
  public function getPlugin(): BlockPluginInterface;

  /**
   * Returns the plugin ID.
   *
   * @return string
   *   The plugin ID for this block.
   */
  public function getPluginId(): string;

  /**
   * Returns an array of visibility condition configurations.
   *
   * @return mixed[]
   *   An array of visibility condition configuration keyed by the condition ID.
   */
  public function getVisibility(): array;

  /**
   * Gets conditions for this block.
   *
   * @return \Drupal\Core\Condition\ConditionPluginCollection
   *   An array or collection of configured condition plugins.
   */
  public function getVisibilityConditions(): ConditionPluginCollection;

  /**
   * Gets a visibility condition plugin instance.
   *
   * @param string $instance_id
   *   The condition plugin instance ID.
   *
   * @return \Drupal\Core\Condition\ConditionInterface
   *   A condition plugin.
   */
  public function getVisibilityCondition($instance_id): ConditionInterface;

  /**
   * Sets the visibility condition configuration.
   *
   * @param string $instance_id
   *   The condition instance ID.
   * @param mixed[] $configuration
   *   The condition configuration.
   *
   * @return $this
   */
  public function setVisibilityConfig($instance_id, array $configuration): HtmxBlockInterface;

  /**
   * Creates a duplicate of the block entity.
   *
   * @param string $new_id
   *   (optional) The new ID on the duplicate block.
   *
   * @return static
   *   A clone of $this with all identifiers unset, so saving it inserts a new
   *   entity into the storage system.
   */
  public function createDuplicateBlock($new_id = NULL): HtmxBlockInterface;

}
