<?php

declare(strict_types=1);

namespace Drupal\htmx\Entity;

use Drupal\Component\Plugin\LazyPluginCollection;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Condition\ConditionInterface;
use Drupal\Core\Condition\ConditionPluginCollection;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Executable\ExecutableManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\block\BlockPluginCollection;

/**
 * Defines the htmx block entity type.
 *
 * @ConfigEntityType(
 *   id = "htmx_block",
 *   label = @Translation("HTMX Block"),
 *   label_collection = @Translation("HTMX Blocks"),
 *   label_singular = @Translation("HTMX block"),
 *   label_plural = @Translation("HTMX blocks"),
 *   label_count = @PluralTranslation(
 *     singular = "@count HTMX block",
 *     plural = "@count HTMX blocks",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\htmx\Entity\HtmxBlockListBuilder",
 *     "access" = "Drupal\block\BlockAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\htmx\Form\HtmxBlockForm",
 *       "edit" = "Drupal\htmx\Form\HtmxBlockForm",
 *       "delete" = "Drupal\htmx\Form\HtmxBlockDeleteForm",
 *     },
 *   },
 *   config_prefix = "htmx_block",
 *   admin_permission = "administer htmx_block",
 *   links = {
 *     "collection" = "/admin/structure/htmx-block",
 *     "edit-form" = "/htmx/blocks/edit/{htmx_block}",
 *     "delete-form" = "/htmx/blocks/delete/{htmx_block}",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "provider",
 *     "plugin",
 *     "settings",
 *     "visibility",
 *   },
 * )
 */
final class HtmxBlock extends ConfigEntityBase implements HtmxBlockInterface {

  /**
   * The example ID.
   */
  protected string $id;

  /**
   * The plugin instance settings.
   *
   * @var string[]
   */
  protected array $settings = [];

  /**
   * The plugin instance ID.
   */
  protected string $plugin;

  /**
   * The visibility settings for this block.
   *
   * @var mixed[]
   */
  protected array $visibility = [];

  /**
   * The plugin collection that holds the block plugin for this entity.
   */
  protected ?BlockPluginCollection $pluginCollection = NULL;

  /**
   * The available contexts for this block and its visibility conditions.
   *
   * @var mixed[]
   */
  protected array $contexts = [];

  /**
   * The visibility collection.
   */
  protected ?ConditionPluginCollection $visibilityCollection = NULL;

  /**
   * The condition plugin manager.
   */
  protected ?ExecutableManagerInterface $conditionPluginManager = NULL;

  /**
   * {@inheritdoc}
   */
  public function getPlugin(): BlockPluginInterface {
    return $this->getPluginCollection()->get($this->plugin);
  }

  /**
   * Encapsulates the creation of the block's LazyPluginCollection.
   *
   * @return \Drupal\Component\Plugin\LazyPluginCollection
   *   The block's plugin collection.
   */
  protected function getPluginCollection(): LazyPluginCollection {
    if (!($this->pluginCollection instanceof BlockPluginCollection)) {
      $this->pluginCollection = new BlockPluginCollection(\Drupal::service('plugin.manager.block'), $this->plugin, $this->get('settings'), $this->id());
    }
    return $this->pluginCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections(): array {
    return [
      'settings' => $this->getPluginCollection(),
      'visibility' => $this->getVisibilityConditions(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId(): string {
    return $this->plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    $settings = $this->get('settings');
    if ($settings['label']) {
      $label = $settings['label'];
    }
    else {
      $definition = $this->getPlugin()->getPluginDefinition();
      $label = $definition['admin_label'] ?? '';
    }
    if ($label instanceof TranslatableMarkup) {
      return $label->render();
    }
    return $label;
  }

  /**
   * {@inheritdoc}
   */
  public function getVisibility(): array {
    return $this->getVisibilityConditions()->getConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function setVisibilityConfig($instance_id, array $configuration): HtmxBlock {
    $conditions = $this->getVisibilityConditions();
    if (!$conditions->has($instance_id)) {
      $configuration['id'] = $instance_id;
      $conditions->addInstanceId($instance_id, $configuration);
    }
    else {
      $conditions->setInstanceConfiguration($instance_id, $configuration);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getVisibilityConditions(): ConditionPluginCollection {
    if (!($this->visibilityCollection instanceof ConditionPluginCollection)) {
      $this->visibilityCollection = new ConditionPluginCollection($this->conditionPluginManager(), $this->get('visibility'));
    }
    return $this->visibilityCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function getVisibilityCondition($instance_id): ConditionInterface {
    return $this->getVisibilityConditions()->get($instance_id);
  }

  /**
   * Gets the condition plugin manager.
   *
   * @return \Drupal\Core\Executable\ExecutableManagerInterface
   *   The condition plugin manager.
   */
  protected function conditionPluginManager(): ExecutableManagerInterface {
    if (!($this->conditionPluginManager instanceof ExecutableManagerInterface)) {
      $this->conditionPluginManager = \Drupal::service('plugin.manager.condition');
    }
    return $this->conditionPluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public function createDuplicateBlock($new_id = NULL): HtmxBlock {
    $duplicate = parent::createDuplicate();
    if (!empty($new_id)) {
      $duplicate->id = $new_id;
    }
    return $duplicate;
  }

}
