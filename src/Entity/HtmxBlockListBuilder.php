<?php

declare(strict_types=1);

namespace Drupal\htmx\Entity;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\htmx\Template\HtmxAttribute;

/**
 * Provides a listing of htmx blocks.
 */
final class HtmxBlockListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\htmx\Entity\HtmxBlockInterface $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['status'] = $entity->status() ? $this->t('Enabled') : $this->t('Disabled');
    return $row + parent::buildRow($entity);
  }

  /**
   * Build the list render array.
   *
   * @return mixed[]
   *   The render array.
   */
  public function render(): array {
    // Create an HTMX powered button.
    $htmxAdd = new HtmxAttribute([
      'class' => [
        'button',
        'button--action',
        'button--primary',
        'button--htmx',
        'button--htmx-refresh',
      ],
      'aria-live' => 'polite',
      'name' => 'open-plugin-dialog',
    ]);
    $htmxAdd->get(Url::fromRoute('htmx_blocks.htmx_system_block_library'))
      ->select('#drupal-off-canvas-wrapper')
      ->target('main')
      ->swap('afterbegin')
      ->on('::afterOnLoad', 'setListWithDialogOpen(this)')
      ->on('pluginDialogRemoved', 'setListWithoutDialog(this)');
    $button['button-add'] = [
      '#type' => 'inline_template',
      '#template' => '<button {{ attributes }}><span class="default-text">{{ add }}</span> <span class="htmx-indicator">{{ load }}</span></button>',
      '#context' => [
        'add' => $this->t('Add a block'),
        'load' => 'Loading...',
        'attributes' => $htmxAdd,
      ],
    ];

    // Build the list.
    $table = parent::render();
    // Assemble the parts.
    // @todo recreate the filter JS without JQuery.
    $build = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'htmx-block-list-container',
        ],
      ],
      'button' => $button,
      'list-container' => [
        '#type' => 'container',
        '#attributes' => [
          'id' => 'htmx-block-list',
        ],
        'list' => $table,
      ],
      '#attached' => [
        'library' => [
          'core/drupal.dropbutton',
          'htmx/drupal',
          'htmx/debug',
          'htmx/dialog.off-canvas',
          'block/drupal.block.admin',
        ],
      ],
    ];
    return $build;
  }

  /**
   * Build  a render array for the operations of a single entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Operations will be built for this entity.
   *
   * @return mixed[]
   *   The render array.
   */
  public function buildOperations(EntityInterface $entity) {
    $htmx = new HtmxAttribute();
    $htmx->boost(TRUE);
    $build = [
      '#type' => 'operations',
      '#links' => $this->getOperations($entity),
      '#attributes' => $htmx->toArray(),
    ];

    return $build;
  }

  /**
   * Build the operations render array with HTMX properties..
   *
   * @return mixed[]
   *   The render array.
   */
  public function getDefaultOperations(EntityInterface $entity): array {
    $operations = [];
    $htmx = new HtmxAttribute();
    $htmx->select('#drupal-off-canvas-wrapper')
      ->target('main')
      ->swap('afterbegin')
      ->pushUrl(FALSE);
    if ($entity->access('update')) {
      $operations['edit'] = [
        'title' => $this->t('Edit'),
        'weight' => 10,
        'url' => Url::fromRoute('entity.htmx_block.edit_form', ['htmx_block' => $entity->id()]),
        'attributes' => $htmx->toArray(),
      ];
    }
    if ($entity->access('delete')) {
      $operations['delete'] = [
        'title' => $this->t('Delete'),
        'weight' => 100,
        'url' => Url::fromRoute('entity.htmx_block.delete_form', ['htmx_block' => $entity->id()]),
        'attributes' => $htmx->toArray(),
      ];
    }
    return $operations;
  }

}
