<?php

declare(strict_types=1);

namespace Drupal\htmx\Form;

use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\htmx\Template\HtmxAttribute;

/**
 * Extend the standard form for HTMX.
 */
class HtmxBlockDeleteForm extends EntityDeleteForm {

  /**
   * Extends the entity form actions for HTMX.
   *
   * @param mixed[] $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return mixed[]
   *   The assembled actions render array.
   */
  protected function actions(array $form, FormStateInterface $form_state): array {
    $actions = parent::actions($form, $form_state);
    // Add HTMX properties to the primary submit button.
    $htmx = new HtmxAttribute($actions['submit']['#attributes']);
    $htmx->post(Url::fromRoute('entity.htmx_block.delete_form', ['htmx_block' => $this->entity->id()]))
      ->select('#htmx-block-list')
      ->target('#htmx-block-list')
      ->swap('outerHTML')
      ->on('::afterOnLoad', 'removeOffCanvasDialog(this)');
    $actions['submit']['#attributes'] = $htmx->toArray();

    return $actions;
  }

  /**
   * Build the form.
   *
   * @param mixed[] $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return mixed[]
   *   The assembled render array.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    // Adjust the form for dialog display.
    $question = [
      'question' => [
        '#type' => 'item',
        '#title' => $form['#title'],
      ],
    ];
    unset($form['#title']);
    return array_merge($question, $form);
  }

  /**
   * Alter redirect on form submission.
   *
   * @param mixed[] $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);
    $form_state->setRedirect('htmx_blocks.updated');
  }

}
