<?php

declare(strict_types=1);

namespace Drupal\htmx\Form;

use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\Core\Plugin\FilteredPluginManagerInterface;
use Drupal\Core\Plugin\PluginFormFactoryInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\Core\Url;
use Drupal\htmx\Entity\HtmxBlock;
use Drupal\htmx\Template\HtmxAttribute;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * HTMX Block form.
 */
class HtmxBlockForm extends EntityForm {

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\htmx\Entity\HtmxBlock
   */
  protected $entity;

  /**
   * The HTMX block storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $storage;

  final public function __construct(
    private readonly FilteredPluginManagerInterface $manager,
    private readonly ContextRepositoryInterface $contextRepository,
    private readonly LanguageManagerInterface $language,
    private readonly PluginFormFactoryInterface $pluginFormFactory,
    EntityTypeManagerInterface $entityTypeManager,
  ) {
    $this->storage = $entityTypeManager->getStorage('htmx_block');
    $this->setEntityTypeManager($entityTypeManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): HtmxBlockForm {
    return new static(
      $container->get('plugin.manager.condition'),
      $container->get('context.repository'),
      $container->get('language_manager'),
      $container->get('plugin_form.factory'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Build the entity form..
   *
   * @param mixed[] $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return mixed[]
   *   The form render array.
   */
  public function form(array $form, FormStateInterface $form_state): array {

    // Store the gathered contexts in the form state for other objects to use
    // during form building.
    $form_state->setTemporaryValue('gathered_contexts', $this->contextRepository->getAvailableContexts());

    $form = parent::form($form, $form_state);
    $form['#tree'] = TRUE;
    $form['settings'] = [];
    $pluginForm = $this->getPluginForm($this->entity->getPlugin());
    if ($pluginForm instanceof PluginFormInterface) {
      $subformState = SubformState::createForSubform($form['settings'], $form, $form_state);
      $form['settings'] = $pluginForm->buildConfigurationForm($form['settings'], $subformState);
    }
    $form['visibility'] = $this->buildVisibilityInterface([], $form_state);

    // If creating a new block, calculate a safe default machine name.
    $form['id'] = [
      '#type' => 'machine_name',
      '#maxlength' => 64,
      '#description' => $this->t('A unique name for this block instance. Must be alpha-numeric and underscore separated.'),
      '#default_value' => !$this->entity->isNew() ? $this->entity->id() : $this->getUniqueMachineName(),
      '#machine_name' => [
        'exists' => [HtmxBlock::class, 'load'],
        'replace_pattern' => '[^a-z0-9_.]+',
        'source' => ['settings', 'label'],
      ],
      '#required' => TRUE,
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];
    // Remove the action and method elements: we will post via htmx.
    unset($form['#action']);
    unset($form['#method']);

    return $form;
  }

  /**
   * Validate the form submission.
   *
   * @param mixed[] $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @see \Drupal\block\BlockForm::validateForm()Form
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    // The HTMX Block Entity form puts all block plugin form elements in the
    // settings form element, so just pass that to the block for validation.
    $this->getPluginForm($this->entity->getPlugin())->validateConfigurationForm($form['settings'], SubformState::createForSubform($form['settings'], $form, $form_state));
    $this->validateVisibility($form, $form_state);
  }

  /**
   * Process form submission.
   *
   * @param mixed[] $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @see \Drupal\block\BlockForm::submitForm
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $entity = $this->entity;
    // The Block Entity form puts all block plugin form elements in the
    // settings form element, so just pass that to the block for submission.
    $sub_form_state = SubformState::createForSubform($form['settings'], $form, $form_state);
    // Call the plugin submit handler.
    $block = $entity->getPlugin();
    $this->getPluginForm($block)->submitConfigurationForm($form, $sub_form_state);
    // If this block is context-aware, set the context mapping.
    if ($block instanceof ContextAwarePluginInterface && $block->getContextDefinitions()) {
      $context_mapping = $sub_form_state->getValue('context_mapping', []);
      $block->setContextMapping($context_mapping);
    }

    $this->submitVisibility($form, $form_state);
  }

  /**
   * Save and redirect to the updated data response.
   *
   * @param mixed[] $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return int
   *   Either SAVED_NEW or SAVED_UPDATED, depending on the operation performed.
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);
    $form_state->setRedirect('htmx_blocks.updated');
    return $result;
  }

  /**
   * Generates a unique machine name for an HTMX block.
   *
   * @return string
   *   Returns the unique name.
   */
  public function getUniqueMachineName() {
    $suggestion = $this->entity->getPlugin()->getMachineNameSuggestion();
    $query = $this->storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('id', $suggestion, 'CONTAINS');
    // Adapted from \Drupal\block\BlockRepository::getUniqueMachineName()
    $block_ids = $query->execute();

    $block_ids = array_map(function ($block_id) {
      $parts = explode('.', $block_id);
      return end($parts);
    }, $block_ids);

    // Iterate through potential IDs until we get a new one. E.g.
    // For example, 'plugin', 'plugin_2', 'plugin_3', etc.
    $count = 1;
    $machine_default = $suggestion;
    while (in_array($machine_default, $block_ids)) {
      $machine_default = $suggestion . '_' . ++$count;
    }
    return $machine_default;
  }

  /* Internal methods */

  /**
   * Helper function to independently validate the visibility UI.
   *
   * @param mixed[] $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @see \Drupal\block\BlockForm::validateVisibility
   */
  protected function validateVisibility(array $form, FormStateInterface $form_state): void {
    // Validate visibility condition settings.
    foreach ($form_state->getValue('visibility') as $condition_id => $values) {
      // Allow the condition to validate the form.
      $condition = $form_state->get(['conditions', $condition_id]);
      $condition->validateConfigurationForm($form['visibility'][$condition_id], SubformState::createForSubform($form['visibility'][$condition_id], $form, $form_state));
    }
  }

  /**
   * Extends the entity form actions for HTMX.
   *
   * @param mixed[] $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return mixed[]
   *   The assembled actions render array.
   */
  protected function actions(array $form, FormStateInterface $form_state): array {
    $parentActions = parent::actions($form, $form_state);
    // Add HTMX properties to the primary submit button.
    $htmx = new HtmxAttribute($parentActions['submit']['#attributes']);
    $htmx->select('#htmx-block-list')
      ->target('#htmx-block-list')
      ->swap('outerHTML')
      ->on('::afterOnLoad', 'removeOffCanvasDialog(this)');
    if ($this->entity->isNew()) {
      $pluginID = $this->entity->getPluginId();
      // Set the post path on the primary button.
      $htmx->post(Url::fromRoute('entity.htmx_block.add_form', ['plugin_id' => $pluginID]));
      $parentActions['submit']['#attributes'] = $htmx->toArray();
      // Add a button to save and configure another plugin.
      $htmxContinue = new HtmxAttribute();
      $htmxContinue->post(Url::fromRoute('entity.htmx_block.add_form', ['plugin_id' => $pluginID]))
        ->select('dialog')
        ->target('closest dialog')
        ->swap('outerHTML')
        ->selectOob('#htmx-block-list');
      $actions = [
        'submit' => $parentActions['submit'],
        'submit-continue' => [
          '#type' => 'submit',
          '#button_type' => 'secondary',
          '#value' => $this->t('Save and continue'),
          '#submit' => ['::submitForm', '::save'],
          '#attributes' => $htmxContinue->toArray(),
        ],
      ];
      // Return the expanded actions.
      return $actions;
    }
    // Set the post path on the primary button.
    $htmx->post(Url::fromRoute('entity.htmx_block.edit_form', ['htmx_block' => $this->entity->id()]));
    $parentActions['submit']['#attributes'] = $htmx->toArray();

    if (!empty($parentActions['delete'])) {
      $htmxDelete = new HtmxAttribute($parentActions['delete']['#attributes'] ?? []);
      $htmxDelete->select('#drupal-off-canvas-wrapper')
        ->target('main')
        ->swap('afterbegin')
        ->pushUrl(FALSE);
      // Replace with link type button.
      $parentActions['delete']['#attributes'] = $htmxDelete->toArray();
    }
    // Return the modified defaults.
    return $parentActions;
  }

  /**
   * Helper function for building the visibility UI form.
   *
   * @param mixed[] $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @see \Drupal\block\BlockForm::buildVisibilityInterface()
   *
   * @return mixed[]
   *   The form array with the visibility UI added in.
   */
  protected function buildVisibilityInterface(array $form, FormStateInterface $form_state): array {
    $form['visibility_tabs'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Visibility'),
      '#parents' => ['visibility_tabs'],
      '#attached' => [
        'library' => [
          'block/drupal.block',
        ],
      ],
    ];
    $visibility = $this->entity->getVisibility();
    $definitions = $this->manager->getFilteredDefinitions('block_ui', $form_state->getTemporaryValue('gathered_contexts'), ['block' => $this->entity]);
    foreach ($definitions as $condition_id => $definition) {
      // Don't display the current theme condition.
      if ($condition_id == 'current_theme') {
        continue;
      }
      // Don't display the language condition until we have multiple languages.
      if ($condition_id == 'language' && !$this->language->isMultilingual()) {
        continue;
      }

      /** @var \Drupal\Core\Condition\ConditionInterface $condition */
      $condition = $this->manager->createInstance($condition_id, $visibility[$condition_id] ?? []);
      $form_state->set(['conditions', $condition_id], $condition);
      $condition_form = $condition->buildConfigurationForm([], $form_state);
      $condition_form['#type'] = 'details';
      $condition_form['#title'] = $condition->getPluginDefinition()['label'];
      $condition_form['#group'] = 'visibility_tabs';
      $form[$condition_id] = $condition_form;
    }

    // Disable negation for specific conditions.
    $disable_negation = [
      'entity_bundle:node',
      'language',
      'response_status',
      'user_role',
    ];
    foreach ($disable_negation as $condition) {
      if (isset($form[$condition])) {
        $form[$condition]['negate']['#type'] = 'value';
        $form[$condition]['negate']['#value'] = $form[$condition]['negate']['#default_value'];
      }
    }

    if (isset($form['user_role'])) {
      $form['user_role']['#title'] = $this->t('Roles');
      unset($form['user_role']['roles']['#description']);
    }
    if (isset($form['request_path'])) {
      $form['request_path']['#title'] = $this->t('Pages');
      $form['request_path']['negate']['#type'] = 'radios';
      $form['request_path']['negate']['#default_value'] = (int) $form['request_path']['negate']['#default_value'];
      $form['request_path']['negate']['#title_display'] = 'invisible';
      $form['request_path']['negate']['#options'] = [
        $this->t('Show for the listed pages'),
        $this->t('Hide for the listed pages'),
      ];
    }
    return $form;
  }

  /**
   * Helper function to independently submit the visibility UI.
   *
   * @param mixed[] $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  protected function submitVisibility(array $form, FormStateInterface $form_state): void {
    foreach ($form_state->getValue('visibility') as $condition_id => $values) {
      // Allow the condition to submit the form.
      $condition = $form_state->get(['conditions', $condition_id]);
      $condition->submitConfigurationForm($form['visibility'][$condition_id], SubformState::createForSubform($form['visibility'][$condition_id], $form, $form_state));

      $condition_configuration = $condition->getConfiguration();
      // Update the visibility conditions on the block.
      $this->entity->getVisibilityConditions()
        ->addInstanceId($condition_id, $condition_configuration);
    }
  }

  /**
   * Retrieves the plugin form for a given block and operation.
   *
   * @param \Drupal\Core\Block\BlockPluginInterface $block
   *   The block plugin.
   *
   * @return \Drupal\Core\Plugin\PluginFormInterface|null
   *   The plugin form for the block.
   */
  protected function getPluginForm(BlockPluginInterface $block): ?PluginFormInterface {
    if ($block instanceof PluginWithFormsInterface) {
      return $this->pluginFormFactory->createInstance($block, 'configure');
    }
    return NULL;
  }

}
